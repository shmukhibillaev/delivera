<?php

namespace console\controllers;

use backend\models\DriversSearch;
use cabinet\models\OrdersSearch;
use common\helpers\SendPushNotification;
use common\models\Devices;
use common\models\Drivers;
use common\models\ErrorsLog;
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 5/24/2019
 * Time: 7:47 PM
 */
class PushController extends Controller
{

    public function actionSendDriver($order_id, $channel)
    {
        $order = OrdersSearch::findOne($order_id);
        $links = Drivers::find()->andWhere(['status' => DriversSearch::ACTIVE])->all();
        $orders = [
            'id' => $order->id,
            'totalPrice' => $order->totalPrice,
            'status' => [
                'code' => $order->status,
                'title' => $order->getStatuses()[$order->status]
            ],
            'payment_type' => [
                'code' => $order->payment_type,
                'title' => $order->getPaymentTypes()[$order->payment_type]
            ],
        ];
        foreach ($links as $link) {
            $devices = Devices::find()->select('push_token')->andWhere(['user_id' => $link->id, 'user_type' => Devices::DRIVERS])->distinct()->all();

            foreach ($devices as $device) {
                if ($device->push_token) {
                    try {
                        SendPushNotification::send($device->push_token, $orders, 'Новый заказ!', $channel);
                    } catch (\Exception $e) {
                        $log = new ErrorsLog();
                        $log->message = 'Device deleted: ' . json_encode($device->getAttributes()) . ' Reason: ' . $e->getMessage();
                        $log->save(false);
                        $device->delete();
                        continue;
                    }
                }
            }
        }
    }
}