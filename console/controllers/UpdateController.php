<?php

namespace console\controllers;

use api\response\Message;
use common\models\Attributes;
use common\models\AttributesGroup;
use common\models\AttributesLink;
use common\models\AttributesPrice;
use common\models\Category;
use common\models\DomainCategory;
use common\models\Entity;
use common\models\EntityDomain;
use common\models\Food;
use common\models\FoodLink;
use common\models\Photos;
use Yii;
use yii\base\Exception;
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 5/24/2019
 * Time: 7:47 PM
 */
class UpdateController extends Controller
{

    public function actionAddFood()
    {
//        ini_set('mysql.connect_timeout', 300);
//        ini_set('default_socket_timeout', 300);
        $allEntities = file_get_contents('C:\Users\Shamsidinkhon\Desktop\allBusiness.json');
        $allEntities = json_decode($allEntities);
        $notIncludedEntities = [];
        $photoCount = 1;
        Yii::$app->db->createCommand('SET SESSION wait_timeout = 28800;')->execute();
        foreach ($allEntities as $entity) {
            $time_start = microtime(true);
            echo 'Entity Domain started: ' . $entity->businessName . PHP_EOL;
            if (($entityDomain = EntityDomain::findOne(['name' => $entity->businessName])) == null) {
                $entityDomain = new EntityDomain();
                $entityDomain->name = $entity->businessName;
                $entityDomain->save(false);

            }


            $fileName = "C:\Users\Shamsidinkhon\Downloads\\express\\$entityDomain->name.json";

            if (!file_exists($fileName)) {
                $notIncludedEntities[] = $entity->businessName;
                continue;
            }

            $data = json_decode(file_get_contents($fileName));
            $entityModel = null;
            if ($data) {
                $entityDataSaved = false;
                foreach ($data as $product) {

                    if (!$entityDataSaved) {
                        $arrCategoryDomain = [];
                        foreach ($product->businessType as $categoryDomain) {
                            if (($cdomain = DomainCategory::findOne(['title' => $categoryDomain])) == null) {
                                $cdomain = new DomainCategory();
                                $cdomain->title = $categoryDomain;
                                $cdomain->save(false);
                            }
                            $arrCategoryDomain[] = $cdomain->id;
                        }
                        $entityDomain->category_domain_id = implode(',', $arrCategoryDomain);

                        if (!file_exists(Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id))) {
                            mkdir(Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id), 0777, true);
                        }

                        $ext = pathinfo('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->businessLogo, PATHINFO_EXTENSION);
                        $file = Yii::$app->security->generateRandomString() . '.' . $ext;
                        if (copy('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->businessLogo, Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file))) {
                            $photoModel = new Photos();
                            $photoModel->title = 'Logo ' . $entityDomain->id;
                            $photoModel->real_path = 'uploads/entityImages/' . $entityDomain->id . '/' . $file;
                            $api = Yii::$app->botClient;
                            $request = $api->sendPhoto();
                            $request->chat_id = '@deliveratest';
                            $request->photo = new \api\InputFile(Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file));

                            if ($photoCount % 19 == 0) {
                                echo 'Sleep ...' . PHP_EOL;
                                sleep(60);
                            }

                            $result = $request->send();
                            if ($result instanceof Message) {
                                $photoCount++;
                                $fileSize = 0;
                                foreach ($result->photo as $photo) {
                                    if ($photo->file_size > $fileSize) {
                                        $fileSize = $photo->file_size;
                                        $photoModel->file_id = $photo->file_id;
                                    }
                                }
                            }
                            $photoModel->save(false);
                            $entityDomain->logo = $photoModel->id;
//                            echo 'Logo ' . $entityDomain->id . ' saved' . PHP_EOL;
                        }

                        $entityDomain->save(false);

                        if (($entityModel = Entity::findOne(['name' => $entityDomain->name])) == null) {
                            $entityModel = new Entity();
                            $entityModel->name = $entityDomain->name;
                            $entityModel->entity_domain_id = $entityDomain->id;
                            $entityModel->save(false);
                        }
                        $entityDataSaved = true;
                    }

                    // product

                    $foodModel = new Food();
                    $foodModel->title = $product->productTitle;

                    foreach ($product->productDesc as $item) {
                        if (strpos($item, 'сум') !== false) {
                            $foodModel->extra_info .= '&' . $item;
                            continue;
                        }
                        $foodModel->description .= '&' . $item;
                    }


                    // product image
                    if ($product->productImage and $product->productImage != 'null' && file_exists('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage)) {
                        $ext = pathinfo('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage, PATHINFO_EXTENSION);
                        $file = Yii::$app->security->generateRandomString() . '.' . $ext;
                        if (copy('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage, Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file))) {
                            $photoModel = new Photos();
                            $photoModel->title = 'Image Entity: ' . $entityDomain->id . ' ' . $product->productTitle;
                            $photoModel->real_path = 'uploads/entityImages/' . $entityDomain->id . '/' . $file;
                            $api = Yii::$app->botClient;
                            $request = $api->sendPhoto();
                            $request->chat_id = '@deliveratest';
                            $request->photo = new \api\InputFile(Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file));

                            if ($photoCount % 19 == 0) {
                                echo 'Sleep ...' . PHP_EOL;
                                sleep(60);
                            }

                            $result = $request->send();
                            if ($result instanceof Message) {
                                $photoCount++;
                                $fileSize = 0;
                                foreach ($result->photo as $photo) {
                                    if ($photo->file_size > $fileSize) {
                                        $fileSize = $photo->file_size;
                                        $photoModel->file_id = $photo->file_id;
                                    }
                                }
                            }
                            $photoModel->save(false);
                            $foodModel->image_id = $photoModel->id;
                        }
                    }
                    $foodModel->save(false);

                    $foodLink = new FoodLink();
                    $foodLink->food_id = $foodModel->id;
                    $foodLink->entity_id = $entityModel->id;

                    if (($category = Category::findOne(['title' => $product->category])) == null) {
                        $category = new Category();
                        $category->title = $product->category;
                    }
                    $category->save(false);
                    $foodLink->category_id = $category->id;
                    $foodLink->price = $product->productPrice;
                    $foodLink->save(false);

                    foreach ($product->productAttrLabel as $key => $value) {

                        foreach ($value as $i => $n) {
                            $attributesGroup = AttributesGroup::findOne(['title' => $i]);
                            if ($attributesGroup == null) {
                                $attributesGroup = new AttributesGroup();
                                $attributesGroup->title = $i;
                                $attributesGroup->save(false);
                            }
                            foreach ($n as $item) {
                                if (($attribute = Attributes::findOne(['title' => $item])) == null) {
                                    $attribute = new Attributes();
                                    $attribute->title = $item;
                                    $attribute->save(false);
                                }

                                if ((AttributesLink::findOne(['group_id' => $attributesGroup->id, 'attribute_id' => $attribute->id, 'food_id' => $foodModel->id, 'entity_id' => $entityModel->id])) == null) {
                                    $attrLink = new AttributesLink();
                                    $attrLink->group_id = $attributesGroup->id;
                                    $attrLink->attribute_id = $attribute->id;
                                    $attrLink->food_id = $foodModel->id;
                                    $attrLink->entity_id = $entityModel->id;
                                    $attrLink->save(false);
                                }
                            }


                        }
                    }

                    foreach ($product->productAttr as $item) {
                        $attrLabels = [];
                        foreach ($item as $key => $value) {
                            if ($key == 'price')
                                continue;
                            $attrLabels[] = $value;
                        }
                        $arrCombination = Attributes::find()->andWhere(['title' => $attrLabels])->orderBy('id')->column();
                        $combinationValue = implode('', $arrCombination);
                        if (($combination = AttributesPrice::findOne(['combination' => $combinationValue, 'food_id' => $foodModel->id, 'entity_id' => $entityModel->id, 'price' => $item->price])) == null) {
                            $combination = new AttributesPrice();
                            $combination->combination = $combinationValue;
                            $combination->food_id = $foodModel->id;
                            $combination->entity_id = $entityModel->id;
                            $combination->price = $item->price;
                            $combination->save(false);
                        }

                    }


                }
            }
            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start) / 60;
            echo 'Entity Domain saved: ' . $entityDomain->name . '  ' . $execution_time . ' Mins' . PHP_EOL;
        }
        var_dump($notIncludedEntities);
//        return $allEntities;
    }

    public function actionChangeDir()
    {
        $allEntities = file_get_contents('C:\Users\Shamsidinkhon\Desktop\allBusiness.json');
        $allEntities = json_decode($allEntities);
        foreach ($allEntities as $entity) {
            $time_start = microtime(true);
            echo 'Entity Domain started: ' . $entity->businessName . PHP_EOL;

            $fileName = "C:\Users\Shamsidinkhon\Downloads\\express\\$entity->businessName.json";
            if (!file_exists($fileName))
                continue;
            $data = json_decode(file_get_contents($fileName));

            if ($data) {
                foreach ($data as $product) {

                    if ($product->businessLogo and $product->businessLogo != 'null' && file_exists('C:\Users\Shamsidinkhon\Downloads\express2 - Copy\\' . $product->businessLogo))
                        copy('C:\Users\Shamsidinkhon\Downloads\express2 - Copy\\' . $product->businessLogo, 'C:\Users\Shamsidinkhon\Downloads\blogos\\' . $product->businessLogo);

                    break;
                }

                $time_end = microtime(true);
                $execution_time = ($time_end - $time_start) / 60;
                echo 'Entity Domain saved: ' . $entity->businessName . '  ' . $execution_time . ' Mins' . PHP_EOL;
            }

        }
    }

    public function actionUpdateFood()
    {
        try {
            $foodLinkIds = [];
            $businessName = "MasterFood";
            $photoCount = 1;
            Yii::$app->db->createCommand('SET SESSION wait_timeout = 28800;')->execute();
            $time_start = microtime(true);
            echo 'Entity Domain started: ' . $businessName . PHP_EOL;
            if (($entityDomain = EntityDomain::findOne(['name' => $businessName])) == null) {
                echo "Entity Domain Not fouund" . PHP_EOL;
                die;
            }


            $fileName = "C:\Users\Shamsidinkhon\Downloads\\express\\$entityDomain->name.json";

            $data = json_decode(file_get_contents($fileName));
            $count = 0;
            $entityModel = null;
            if ($data) {
                $entityDataSaved = false;
                foreach ($data as $product) {
                    $count++;
                    if (!$entityDataSaved) {
                        $arrCategoryDomain = [];
                        foreach ($product->businessType as $categoryDomain) {
                            if (($cdomain = DomainCategory::findOne(['title' => $categoryDomain])) == null) {
                                echo "Domain Category not found" . PHP_EOL;
                                die;
                            }
                            $arrCategoryDomain[] = $cdomain->id;
                        }
                        $entityDomain->category_domain_id = implode(',', $arrCategoryDomain);

                        if (($entityModel = Entity::findOne(['name' => "MasterFood (Хадра)"])) == null) {
                            echo "Entity Not found" . PHP_EOL;
                            die;
                        }
                        $entityDataSaved = true;
                    }

                    // product


                    $foodModel = new Food();
                    $foodModel->title = $product->productTitle;
                    $newFoodAdding = true;

                    foreach ($product->productDesc as $item) {
                        if (strpos($item, 'сум') !== false) {
                            $foodModel->extra_info .= '&' . $item;
                            continue;
                        }
                        $foodModel->description .= '&' . $item;
                    }
                    if (count($oldFoods = Food::findAll(['title' => $foodModel->title, 'description' => $foodModel->description])) > 0) {
                        $entityFoods = FoodLink::find()->select('food_id')->andWhere(['entity_id' => $entityModel->id])->column();
                        foreach ($oldFoods as $oldFood) {
                            if (in_array($oldFood->id, $entityFoods)) {
                                $foodModel = $oldFood;
                                $newFoodAdding = false;
                                break;
                            }
                        }
                    }

                    // product image
                    if ($newFoodAdding && $product->productImage and $product->productImage != 'null' && file_exists('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage)) {
                        $ext = pathinfo('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage, PATHINFO_EXTENSION);
                        $file = Yii::$app->security->generateRandomString() . '.' . $ext;
                        if (copy('C:\Users\Shamsidinkhon\Downloads\express\\' . $product->productImage, Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file))) {
                            $photoModel = new Photos();
                            $photoModel->title = 'Image Entity: ' . $entityDomain->id . ' ' . $product->productTitle;
                            $photoModel->real_path = 'uploads/entityImages/' . $entityDomain->id . '/' . $file;
                            $api = Yii::$app->botClient;
                            $request = $api->sendPhoto();
                            $request->chat_id = '@deliveratest';
                            $request->photo = new \api\InputFile(Yii::getAlias('@frontend/web/uploads/entityImages/' . $entityDomain->id . '/' . $file));

                            if ($photoCount % 19 == 0) {
                                echo 'Sleep ...' . PHP_EOL;
                                sleep(60);
                            }

                            $result = $request->send();
                            if ($result instanceof Message) {
                                $photoCount++;
                                $fileSize = 0;
                                foreach ($result->photo as $photo) {
                                    if ($photo->file_size > $fileSize) {
                                        $fileSize = $photo->file_size;
                                        $photoModel->file_id = $photo->file_id;
                                    }
                                }
                            }
                            $photoModel->save(false);
                            $foodModel->image_id = $photoModel->id;
                        }
                    }
                    if ($newFoodAdding)
                        $foodModel->save(false);

                    $foodLink = new FoodLink();
                    $foodLink->food_id = $foodModel->id;
                    $foodLink->entity_id = $entityModel->id;

                    if (($category = Category::findOne(['title' => $product->category])) == null) {
                        $category = new Category();
                        $category->title = $product->category;
                    }
                    $category->save(false);
                    $foodLink->category_id = $category->id;
                    if (($oldFoodLink = FoodLink::findOne(['food_id' => $foodModel->id, 'entity_id' => $entityModel->id, 'category_id' => $category->id])) != null) {
                        $foodLink = $oldFoodLink;
                    }
                    $foodLink->price = $product->productPrice;
                    $foodLink->save(false);
                    $foodLinkIds[] = $foodLink->id;
                    foreach ($product->productAttrLabel as $key => $value) {

                        foreach ($value as $i => $n) {
                            $attributesGroup = AttributesGroup::findOne(['title' => $i]);
                            if ($attributesGroup == null) {
                                $attributesGroup = new AttributesGroup();
                                $attributesGroup->title = $i;
                                $attributesGroup->save(false);
                            }
                            foreach ($n as $item) {
                                if (($attribute = Attributes::findOne(['title' => $item])) == null) {
                                    $attribute = new Attributes();
                                    $attribute->title = $item;
                                    $attribute->save(false);
                                }

                                if ((AttributesLink::findOne(['group_id' => $attributesGroup->id, 'attribute_id' => $attribute->id, 'food_id' => $foodModel->id, 'entity_id' => $entityModel->id])) == null) {
                                    $attrLink = new AttributesLink();
                                    $attrLink->group_id = $attributesGroup->id;
                                    $attrLink->attribute_id = $attribute->id;
                                    $attrLink->food_id = $foodModel->id;
                                    $attrLink->entity_id = $entityModel->id;
                                    $attrLink->save(false);
                                }
                            }


                        }
                    }

                    foreach ($product->productAttr as $item) {
                        $attrLabels = [];
                        foreach ($item as $key => $value) {
                            if ($key == 'price')
                                continue;
                            $attrLabels[] = $value;
                        }
                        $arrCombination = Attributes::find()->andWhere(['title' => $attrLabels])->orderBy('id')->column();
                        $combinationValue = implode('', $arrCombination);
                        if (($combination = AttributesPrice::findOne(['combination' => $combinationValue, 'food_id' => $foodModel->id, 'entity_id' => $entityModel->id])) == null) {
                            $combination = new AttributesPrice();
                            $combination->combination = $combinationValue;
                            $combination->food_id = $foodModel->id;
                            $combination->entity_id = $entityModel->id;
                        }
                        $combination->price = $item->price;
                        $combination->save(false);

                    }


                }
            }
            $deleteIds = FoodLink::find()->select('id')->andWhere(['not in', 'id', $foodLinkIds])->andWhere(['entity_id' => $entityModel->id])->column();
            FoodLink::deleteAll(['id' => $deleteIds]);
            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start) / 60;
            echo "Products: " . $count . PHP_EOL;
            echo 'Entity Domain saved: ' . $entityDomain->name . '  ' . $execution_time . ' Mins' . PHP_EOL;

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function actionDuplicateEntity(){
        $source_id = 27;
        $target_id = 129;
        $attrLinks = AttributesLink::findAll(['entity_id' => $source_id]);
        foreach ($attrLinks as $attrLink){
            $model = new AttributesLink();
            $model->setAttributes($attrLink->attributes);
            $model->entity_id = $target_id;
            $model->save();
        }
        $attrLinks = AttributesPrice::findAll(['entity_id' => $source_id]);
        foreach ($attrLinks as $attrLink){
            $model = new AttributesPrice();
            $model->setAttributes($attrLink->attributes);
            $model->entity_id = $target_id;
            $model->save();
        }

        $attrLinks = FoodLink::findAll(['entity_id' => $source_id]);
        foreach ($attrLinks as $attrLink){
            $model = new FoodLink();
            $model->setAttributes($attrLink->attributes);
            $model->entity_id = $target_id;
            $model->save();
        }
        echo "Done";
    }
}