<?php

namespace console\controllers;

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use common\models\Photos;
use yii\console\Controller;

/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 5/24/2019
 * Time: 7:47 PM
 */
class TestController extends Controller
{

    public function actionPhoto(){
        $photos = Photos::find()->andWhere(['url' => null])->all();
        foreach ($photos as $photo){
            if(!file_exists('C:\Users\Shamsidinkhon\OneDrive\Projects\delivera\frontend\web\\' . str_replace('/', '\\', $photo->real_path))) {
                echo $photo->id . ' - Not saved' . PHP_EOL;
                continue;
            }
            $bucket = 'delivera.uploads';
            $keyname = $photo->file_id;

            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => 'us-east-2',
                'credentials' => [
                    'key'    => "AKIAJDGX5DDHBJ5NZE2Q",
                    'secret' => "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",
                ]
            ]);

            try {
                // Upload data.
                $result = $s3->putObject([
                    'Bucket' => $bucket,
                    'Key'    => $keyname,
                    'SourceFile' => 'C:\Users\Shamsidinkhon\OneDrive\Projects\delivera\frontend\web\\' . str_replace('/', '\\', $photo->real_path),
                    'ACL'    => 'public-read'
                ]);

                $result = $result->toArray();
                $photo->aws_result = json_encode($result);
                $photo->url = $result['ObjectURL'];
                $photo->save(false);
                echo $photo->id . ' - Saved' . PHP_EOL;
            } catch (S3Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
        echo 'All Saved';
    }

}