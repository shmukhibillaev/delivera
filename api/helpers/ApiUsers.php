<?php
/**
 * Created by PhpStorm.
 * User: s_muxibillayev
 * Date: 18.12.2018
 * Time: 8:11
 */

namespace api\helpers;


use common\components\user\Drivers;
use common\models\Devices;
use common\models\EntityTokens;
use common\models\User;
use Yii;

class ApiUsers
{
    const USERNAME = 'delivera';
    const PASSWORD = 'X19WkHHupFJBPsMRPCJwTbv09yCD50E2';

    public static function checkAPIUser($username, $password)
    {
        return self::USERNAME == $username && self::PASSWORD == $password;
    }

    public static function loginUser($device, $user_type)
    {
        if ($device) {
            $user = null;
            if ($user_type == Devices::DRIVERS) {
                $user = Drivers::findOne(['id' => $device->user_id]);
                if ($user)
                    Yii::$app->user->login($user);
            }

            if (in_array($user_type, [Devices::ENTITY, Devices::CLIENT])) {
                $user = User::findOne(['id' => $device->user_id]);
                if ($user) {
                    Yii::$app->user->identityClass = 'common\models\User';
                    Yii::$app->user->login($user);
                }
            }

        }
    }

    public static function checkEntityToken($entity_id, $token){
        return EntityTokens::find()->andWhere(['entity_id' => (int)$entity_id, 'token' => $token])->exists();
    }

}