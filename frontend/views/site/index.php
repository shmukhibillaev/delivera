<?php

/* @var $this yii\web\View */

$this->title = 'Delivera | Доставка еды со скоростью света';
?>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-white pt-5">
        <a style="margin: 0 auto;" class="navbar-brand" href="#">
            <?= \yii\helpers\Html::img('@web/images/logo.png')?>
<!--            <img alt="delivera" src="images/logo.png" />-->
        </a>
        <button class="navbar-toggler d-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a style="color: #2c2929" class="nav-link" href="tel:+998974494493">Телефон поддержки: +998 97 449-44-93</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row">
        <div class="col-lg-8 offset-lg-2 col-12 py-5 my-5 py-lg-0 my-lg-0 pb-lg-5">
            <?= \yii\helpers\Html::img('@web/images/teaser-test.png')?>
<!--            <img class="img-fluid" src="images/teaser-test.png" />-->
        </div>
        <div>

        </div>
    </div>
</div>
<div class="container-fluid" style="background: #5FAB5A">
    <div class="row">
        <div class="container">
            <div class="top-part">
                <h1 class="text-white text-center pt-5">Подписывайтесь на нас в соцсетях!</h1>
                <p class="text-white text-center">И будьте всегда в курсе всех новостей</p>
                <div class="row">
                    <div class="col-12 col-lg-4 offset-lg-4">
                        <div class="d-flex justify-content-around">
                            <a target="_blank" href="https://t.me/delivera">
                                <?= \yii\helpers\Html::img('@web/images/tg.png')?>
<!--                                <img src="images/tg.png" alt="telegram"/>-->
                            </a>
                            <a target="_blank" href="https://instagram.com/delivera.uz">
                                <?= \yii\helpers\Html::img('@web/images/insta.png')?>
<!--                                <img src="images/insta.png" alt="instagram" />-->
                            </a>
                            <a target="_blank" href="https://facebook.com/delivera.uz">
                                <?= \yii\helpers\Html::img('@web/images/fb.png')?>
<!--                                <img src="images/fb.png" alt="facebook"/>-->
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-part pt-5 mt-0 mt-lg-5">
                <div class="row">
                    <div class="col-12 col-lg-8 offset-lg-2">
                        <div class="d-flex justify-content-center">
                            <?= \yii\helpers\Html::img('@web/images/headset.png')?>
<!--                            <img alt="operator image" src="images/headset.png"/>-->
                        </div>
                        <h1 class="text-center text-white py-3">Нужна помощь?</h1>
                        <p class="text-center text-white">Свяжитесь с нами по телефонному номеру или по почте <br /> и мы с радостью ответим на все Ваши вопросы :)</p>
                    </div>
                </div>
            </div>
            <div class="footer-part pb-4">
                <div class="row">
                    <div class="col-12 col-lg-4 offset-lg-4">
                        <div class="d-flex justify-content-around aling-items-center">
                            <p class="m-0 pt-1"><a href="tel:+998974494493">+998 97 449-44-93</a></p>
                            <?= \yii\helpers\Html::img('@web/images/rectangle.png')?>
<!--                            <img src="images/rectangle.png" />-->
                            <p class="m-0 pt-1"><a href="mailto:info@delivera.uz">info@delivera.uz</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
