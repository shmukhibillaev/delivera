<?php

use api\base\API;
use common\models\BotUsers;

/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 11/25/2018
 * Time: 6:16 PM
 * @property API $api
 * @property BotUsers $user
 */


namespace frontend\models;


use api\base\Curl;
use api\keyboard\button\InlineKeyboardButton;
use api\keyboard\button\KeyboardButton;
use api\keyboard\InlineKeyboardMarkup;
use api\keyboard\ReplyKeyboardMarkup;
use backend\controllers\FoodLinkController;
use common\helpers\GeneralHelper;
use common\helpers\SendSMSHelper;
use common\models\Attributes;
use common\models\AttributesGroup;
use common\models\AttributesLink;
use common\models\AttributesPrice;
use common\models\Basket;
use common\models\Category;
use common\models\ConfirmCodes;
use common\models\DomainCategory;
use common\models\Entity;
use common\models\EntityDomain;
use common\models\ErrorsLog;
use common\models\Food;
use common\models\FoodLink;
use common\models\Photos;
use common\models\StatesLog;
use common\models\TempBasket;
use DateTime;
use Yii;
use yii\db\Exception;

class Commands
{
    public $command;
    public $message;
    public $api;
    public $user;
    public $userState;
    public $commands = [
        '/start' => 'Start',
        "\u{1F1FA}\u{1F1FF} O'zbek" => "changeToUzbek",
        "\u{1F1F7}\u{1F1FA} Русский" => "changeToRussian",
        "\u{1F91D} Я согласен(сна)" => "changeToRussian",
    ];

    public $basicCommands = [
        'Back',
        'Start',
        'changeToUzbek',
        'changeToRussian',
        'Order',
        'Basket',
        'ConditionDelivery',
        'changePhone',
        'MainMenu'
    ];

    public function __construct($command)
    {
        $this->commands["\u{1F37D} " . Yii::t('main', 'Заказать')] = "Order";
        $this->commands["\u{2B05} " . Yii::t('main', 'Назад')] = "Back";
        $this->commands["\u{1F6D2} " . Yii::t('main', 'Корзина')] = "Basket";
        $this->commands["\u{1F696} " . Yii::t('main', 'Условия доставки')] = "ConditionDelivery";
        $this->commands["\u{1F4DE} " . Yii::t('main', 'Отправить другой номер')] = "changePhone";
        $this->commands["\u{1F4DE} " . Yii::t('main', 'Изменить номер телефона')] = "changePhoneSettings";
        $this->commands["\u{2699} " . Yii::t('main', 'Настройки')] = "Settings";
        $this->commands["\u{1F30D} " . Yii::t('main', 'Изменить язык')] = "changeLang";
        $this->commands["\u{23EE} " . Yii::t('main', 'Главное меню')] = "MainMenu";
        $this->command = array_key_exists($command, $this->commands) ? $this->commands[$command] : $command;
        $this->api = Yii::$app->botClient;
    }

    public function process()
    {
        $func = $this->command;
        $params = null;
        if (!in_array($this->command, $this->basicCommands) && ($state = StatesLog::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one()) != null) {
            $this->userState = $state;

            switch ($state->states) {
                case StatesLog::CHANGE_LANG:
                    $params = $this->command;
                    $func = $this->command = 'changeLanguage';
                    break;
                case StatesLog::CODE_SEND:
                    $params = $this->command;
                    $func = $this->command = 'checkCode';
                    break;
                case StatesLog::PHONE_SEND:
                    $func = $this->command = 'afterPhoneSent';
                    break;
                case StatesLog::PHONE_APPROVE:
                    $params = $this->command;
                    $func = $this->command = 'beforePhoneSent';
                    break;
                case StatesLog::PHONE_APPROVE_SETTINGS:
                    $params = $this->command;
                    $func = $this->command = 'beforePhoneSentSettings';
                    break;
                case StatesLog::CATEGORY_DOMAIN_LIST:
                    $command = explode(" ", $this->command);
                    array_shift($command);
                    $categoryId = DomainCategory::findOne(['title' => implode(" ", $command)]);
                    $func = $this->command = 'SendEntityList';
                    $params = @$categoryId->id;
                    break;
                case StatesLog::ENTITY_LIST:
                    $command = explode(" ", $this->command);
                    array_shift($command);
                    $entityId = EntityDomain::findOne(['name' => implode(" ", $command)])->id;

                    $location = explode(',', $this->user->location);
                    $connection = Yii::$app->db;
                    $db = $connection->createCommand("
                    SELECT
                      id,
                      (6371 * acos(cos(radians({$location[0]})) * cos(radians(latitude)) * cos(radians(longitude) - radians({$location[1]})) +
                                   sin(radians({$location[0]})) * sin(radians(latitude)))) AS distance
                    FROM entity
                      WHERE entity_domain_id = {$entityId} AND active = 1
                    HAVING distance <= 12
                    ORDER BY distance
                    LIMIT 0, 1;
                    ");
                    $result = $db->queryOne();
                    $params = $result['id'];
                    $this->user->location_distance = number_format($result['distance'], 2);
                    $this->user->save(false);
                    $func = $this->command = 'SendCategories';
                    break;
                case StatesLog::SEND_LOCATION:
                    //saving user location latitude,longitude
                    $this->user->location = $this->message->location->latitude . "," . $this->message->location->longitude;
                    $this->user->save(false);
//                    $connection = Yii::$app->db;
//                    $db = $connection->createCommand("
//                    SELECT
//                      id,
//                      (6371 * acos(cos(radians({$this->message->location->latitude})) * cos(radians(latitude)) * cos(radians(longitude) - radians({$this->message->location->longitude})) +
//                                   sin(radians({$this->message->location->latitude})) * sin(radians(latitude)))) AS distance
//                    FROM entity
//                      WHERE entity_domain_id = {$state->params} AND active = 1
//                    HAVING distance < 30
//                    ORDER BY distance
//                    LIMIT 0, 1;
//                    ");
//                    $result = $db->queryOne();
//                    $params = $result['id'];
//                    if(!$params){
//                        $func = $this->command = 'SendEmptyCategories';
//                        break;
//                    }
//                    $this->user->location_distance = number_format($result['distance'], 2);
//                    $this->user->save(false);
//                    $func = $this->command = 'SendCategories';
                    $func = $this->command = 'SendCategoryDomainList';
                    break;
                case StatesLog::CATEGORY_LIST:
                    $command = explode(" ", $this->command);
                    array_shift($command);
                    $categoryId = Category::findOne(['title' => implode(" ", $command)]);
                    $func = $this->command = 'SendFoods';
                    $params = @$categoryId->id;
                    break;
                case StatesLog::FOOD_LIST:
                    $command = explode(" ", $this->command);
                    array_shift($command);
//                    $params = Food::findOne(['title' => implode(" ", $command)]);
                    $params = implode(" ", $command);
                    $func = $this->command = 'SendFood';
                    break;
                case StatesLog::ATTRIBUTE_LIST:
                    $func = 'SendAttributes';
                    break;
                case StatesLog::FOOD_QUANTITY:
                    $func = 'AddBasket';
                    break;
            }
        }

        $params ? $this->$func($params) : $this->$func();
    }

    public function Start()
    {
//        if ($this->user->phone) {
//            $this->afterPhoneSent();
//            return true;
//        }
        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
//        $request->text = "Assalomu Aleykum! Здравствуйте!" . PHP_EOL
//            . "<b>" . $this->user->first_name . " " . $this->user->last_name . "</b>" . PHP_EOL
//            . "\u{1F1FA}\u{1F1FF} Iltimos davom ettirish uchun tilni tanlang!" . PHP_EOL
//            . "\u{1F1F7}\u{1F1FA} Пожалуйста, выберите язык чтобы продолжить";
        $request->text = "Здравствуйте!" . PHP_EOL
            . "<b>" . $this->user->first_name . " " . $this->user->last_name . "</b>" . PHP_EOL
            . "подтвердите своё согласие с пользовательским соглашением. Вы можете ознакомиться с пользовательским соглашением пройдя по ссылке:" . PHP_EOL
            . "https://delivera.uz/agreement";
        $request->parse_mode = 'HTML';

        $keyboard = new ReplyKeyboardMarkup();
//        $keyboard->keyboard = [
//            [['text' => "\u{1F1FA}\u{1F1FF} O'zbek"]],
//            [['text' => "\u{1F1F7}\u{1F1FA} Русский"]],
//        ];
        $keyboard->keyboard = [
            [['text' => "\u{1F91D} Я согласен(сна)"]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function changeToUzbek()
    {

        $this->user->lang = 'uz';
        $this->user->save(false);
        Yii::$app->language = 'uz';
        if ($this->user->phone) {
            $this->afterPhoneSent();
            return true;
        }

        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::PHONE_APPROVE, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста отправьте свой мобильный номер чтобы связаться с вами!') . PHP_EOL;
        $request->text .= Yii::t('main', 'Если у вас есть другой номер для связи, просто отправьте нам в виде: <b>998911234567</b>') . PHP_EOL;
        $request->text .= Yii::t('main', 'Затем мы вышлем вам <b>код</b> для подтверждения!');
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить номер телефона'), 'request_contact' => true]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function changeToRussian()
    {
        $this->user->lang = 'ru';
        $this->user->save(false);
        Yii::$app->language = 'ru';
        if ($this->user->phone) {
            $this->afterPhoneSent();
            return true;
        }

        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::PHONE_APPROVE, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста отправьте свой мобильный номер чтобы связаться с вами!') . PHP_EOL;
        $request->text .= Yii::t('main', 'Если у вас есть другой номер для связи, просто отправьте нам в виде: <b>998911234567</b>') . PHP_EOL;
        $request->text .= Yii::t('main', 'Затем мы вышлем вам <b>код</b> для подтверждения!');
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить номер телефона'), 'request_contact' => true]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function changePhone()
    {
        if ($this->user->phone) {
            $this->afterPhoneSent();
            return true;
        }
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::PHONE_APPROVE, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста отправьте свой мобильный номер чтобы связаться с вами!') . PHP_EOL;
        $request->text .= Yii::t('main', 'Если у вас есть другой номер для связи, просто отправьте нам в виде: <b>998911234567</b>') . PHP_EOL;
        $request->text .= Yii::t('main', 'Затем мы вышлем вам <b>код</b> для подтверждения!');
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить номер телефона'), 'request_contact' => true]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function beforePhoneSent($params = null)
    {
        if ($this->user->phone) {
            $this->afterPhoneSent();
            return true;
        }
        if (!$this->user->phone) {
            $sentCodes = ConfirmCodes::findAll(['user_id' => $this->user->id]);
            if (count($sentCodes) >= 5) {
                $request = $this->api->sendMessage();
                $request->chat_id = $this->user->telegram_id;
                $request->text = Yii::t('main', 'Вы превысили лимит отправки кодов подтверждения! Пожалуйста свяжитесь с нами. +99891 361 84 62');
                $request->send();
                return;
            }
            $code = rand(10000, 100000);
            if (isset($this->message->contact->phone_number)) {
                if (preg_match("/^\+998\d{9}$/", $this->message->contact->phone_number)) {
                    $phone = explode('+', $this->message->contact->phone_number);
                    $phone = $phone[1];
                    SendSMSHelper::sendSMS($phone, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                    ConfirmCodes::log($this->user->id, $code, $phone);
                } elseif (preg_match("/^998\d{9}$/", $this->message->contact->phone_number)) {
                    SendSMSHelper::sendSMS($this->message->contact->phone_number, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                    ConfirmCodes::log($this->user->id, $code, $this->message->contact->phone_number);
                } else {
                    $request = $this->api->sendMessage();
                    $request->chat_id = $this->user->telegram_id;
                    $request->text = Yii::t('main', 'Номер телефона должен быть как в примере: 998911234567!');
                    $request->send();
                    return;
                }
            } else {
                if (is_numeric($params)) {
                    if (preg_match("/^998\d{9}$/", $params)) {
                        SendSMSHelper::sendSMS($params, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                        ConfirmCodes::log($this->user->id, $code, $params);
                    } else {
                        $request = $this->api->sendMessage();
                        $request->chat_id = $this->user->telegram_id;
                        $request->text = Yii::t('main', 'Номер телефона должен быть как в примере: 998911234567!');
                        $request->send();
                        return;
                    }
                } else {
                    $request = $this->api->sendMessage();
                    $request->chat_id = $this->user->telegram_id;
                    $request->text = Yii::t('main', 'Номер телефона должен состоять только из цифр!');
                    $request->send();
                    return;
                }
            }

            $state = new StatesLog();
            $state->log($this->user->id, StatesLog::CODE_SEND, $this->message->message_id, $this->command);

            $request = $this->api->sendMessage();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', 'Пожалуйста, пришлите код подтверждения, который вы получили');

            $keyboard = new ReplyKeyboardMarkup();
            $keyboard->keyboard = [
                [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить другой номер')]],
            ];
            $request->reply_markup = $keyboard;
            $request->send();
        }
    }

    public function beforePhoneSentSettings($params = null)
    {
        $code = rand(10000, 100000);
        if (isset($this->message->contact->phone_number)) {
            if (preg_match("/^\+998\d{9}$/", $this->message->contact->phone_number)) {
                $phone = explode('+', $this->message->contact->phone_number);
                $phone = $phone[1];
                SendSMSHelper::sendSMS($phone, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                ConfirmCodes::log($this->user->id, $code, $phone);
            } elseif (preg_match("/^998\d{9}$/", $this->message->contact->phone_number)) {
                SendSMSHelper::sendSMS($this->message->contact->phone_number, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                ConfirmCodes::log($this->user->id, $code, $this->message->contact->phone_number);
            } else {
                $request = $this->api->sendMessage();
                $request->chat_id = $this->user->telegram_id;
                $request->text = Yii::t('main', 'Номер телефона должен быть как в примере: 998911234567!');
                $request->send();
                return;
            }
        } else {
            if (is_numeric($params)) {
                if (preg_match("/^998\d{9}$/", $params)) {
                    SendSMSHelper::sendSMS($params, Yii::t('main', 'Код подтверждения: {code} delivera.uz', ['code' => $code]));
                    ConfirmCodes::log($this->user->id, $code, $params);
                } else {
                    $request = $this->api->sendMessage();
                    $request->chat_id = $this->user->telegram_id;
                    $request->text = Yii::t('main', 'Номер телефона должен быть как в примере: 998911234567!');
                    $request->send();
                    return;
                }
            } else {
                $request = $this->api->sendMessage();
                $request->chat_id = $this->user->telegram_id;
                $request->text = Yii::t('main', 'Номер телефона должен состоять только из цифр!');
                $request->send();
                return;
            }
        }

        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::CODE_SEND, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста, пришлите код подтверждения, который вы получили');

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить другой номер')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function checkCode($params)
    {
        $code = ConfirmCodes::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        if ($code->code == $params) {
            $this->user->phone = $code->phone;
            $this->user->save(false);
            $this->command = 'afterPhoneSent';
            $this->afterPhoneSent();
        } else {
            $request = $this->api->sendMessage();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', 'Код подтверждения неверен!') . PHP_EOL;
            $request->text .= Yii::t('main', 'Пожалуйста, отправьте еще раз!');
            $request->send();
            return;
        }
    }

    public function afterPhoneSent()
    {

        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::MAIN_MENU, $this->message->message_id, $this->command);

        $request = $this->api->sendPhoto();
        $request->chat_id = $this->user->telegram_id;
        $request->photo = @Photos::findOne(['for_command' => 'afterPhoneSent'])->file_id;
        $request->caption = Yii::t('main', 'Доставка еды и напитков со скоростью света');

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F37D} " . Yii::t('main', 'Заказать')]],
            [['text' => "\u{1F6D2} " . Yii::t('main', 'Корзина')]],
            [['text' => "\u{1F696} " . Yii::t('main', 'Условия доставки')]],
            [['text' => "\u{2699} " . Yii::t('main', 'Настройки')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function MainMenu()
    {

        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::MAIN_MENU, $this->message->message_id, $this->command);

        $request = $this->api->sendPhoto();
        $request->chat_id = $this->user->telegram_id;
        $request->photo = @Photos::findOne(['for_command' => 'afterPhoneSent'])->file_id;
        $request->caption = Yii::t('main', 'Доставка еды и напитков со скоростью света');

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F37D} " . Yii::t('main', 'Заказать')]],
            [['text' => "\u{1F6D2} " . Yii::t('main', 'Корзина')]],
            [['text' => "\u{1F696} " . Yii::t('main', 'Условия доставки')]],
            [['text' => "\u{2699} " . Yii::t('main', 'Настройки')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function Order()
    {
        $this->SendLocation();
    }

    public function SendCategoryDomainList()
    {
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::CATEGORY_DOMAIN_LIST, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Выберите категорию') . " \u{2B07}";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->setKeyboard([]);

        $location = explode(',', $this->user->location);
        $categoryDomains = DomainCategory::findBySql("
        SELECT * FROM `domain_category` dc WHERE EXISTS
        (
            SELECT 
             (6371 * acos(cos(radians({$location[0]})) * cos(radians(e.latitude)) * cos(radians(e.longitude) - radians({$location[1]})) +
                                   sin(radians({$location[0]})) * sin(radians(e.latitude)))) AS distance
             FROM entity_domain ed INNER JOIN entity e ON e.entity_domain_id = ed.id
            WHERE ed.category_domain_id LIKE CONCAT('%', dc.id, '%') AND e.active = 1 AND (CURRENT_TIME() BETWEEN CAST(e.works_from AS time) AND CAST(e.works_to AS time)
        OR (NOT CURRENT_TIME() BETWEEN CAST(e.works_to AS time) AND CAST(e.works_from AS time) AND CAST(e.works_from AS time) >= CAST(e.works_to AS time)))
            HAVING distance <= 12
        ) 
        ORDER BY 'order'
        ")->all();
//        file_put_contents( Yii::getAlias('@frontend') . '/runtime/logs/debug.txt', json_encode($categoryDomains) . PHP_EOL.
//            "-------------------------".PHP_EOL, FILE_APPEND);
        foreach ($categoryDomains as $categoryDomain) {
            $keyboardCount = $keyboard->getKeyboard();
            $button = new KeyboardButton();
            $button->text = "\u{1F449} " . $categoryDomain->title;
            if (count($keyboard->keyboard) == 0) {
                $keyboard->addButton($button);
                continue;
            }
            if (count(end($keyboardCount)) == 1) {
                $keyboard->addButton($button, count($keyboardCount), 2);
            } else {
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
            }
        }
        $keyboardCount = $keyboard->getKeyboard();
        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboardCount) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function SendEntityList($params)
    {

        // category domain id saved to params field
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::ENTITY_LIST, $this->message->message_id, $this->command, $params);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Выберите заведение') . " \u{2B07}";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [];
//        $entityDomains = EntityDomain::find()->andWhere(['like', 'category_domain_id', $params])->orderBy(['id' => SORT_DESC])->all();
        $location = explode(',', $this->user->location);
        $entityDomains = EntityDomain::findBySql("
            SELECT  (6371 * acos(cos(radians({$location[0]})) * cos(radians(e.latitude)) * cos(radians(e.longitude) - radians({$location[1]})) +
                                   sin(radians({$location[0]})) * sin(radians(e.latitude)))) AS distance,
            ed.name, e.id as entity_id FROM entity_domain ed INNER JOIN entity e ON e.entity_domain_id = ed.id
            WHERE ed.category_domain_id LIKE '%{$params}%' AND e.active = 1 AND (CURRENT_TIME() BETWEEN CAST(e.works_from AS time) AND CAST(e.works_to AS time)
        OR (NOT CURRENT_TIME() BETWEEN CAST(e.works_to AS time) AND CAST(e.works_from AS time) AND CAST(e.works_from AS time) >= CAST(e.works_to AS time)))
            HAVING distance <= 12
            ORDER BY ed.id DESC
        ")->all();

        $entityNames = [];
//        file_put_contents( Yii::getAlias('@frontend') . '/runtime/logs/debug.txt', json_encode($entityDomains) . PHP_EOL.
//            "-------------------------".PHP_EOL, FILE_APPEND);
        foreach ($entityDomains as $domain) {
            if(in_array(@$domain->entity_id, [132, 131])){
                if(@$domain->distance > 3)
                    continue;
            }
            if(isset($entityNames[$domain->name]))
                continue;
            else{
                $entityNames[$domain->name] = true;
            }
            $keyboardCount = $keyboard->getKeyboard();
            $button = new KeyboardButton();
            $button->text = "\u{1F3EA} " . $domain->name;
            if (count($keyboardCount) == 0) {
                $keyboard->addButton($button);
                continue;
            }
            if (count(end($keyboardCount)) == 1) {
                $keyboard->addButton($button, count($keyboardCount), 2);
            } else {
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
            }
        }
        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function Back()
    {
        $skip = false;
        $activity = 'afterPhoneSent';
        $params = null;
        $checkSate = StatesLog::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        switch ($checkSate->states) {
            case StatesLog::FOOD_LIST:
                $skip = true;
                $state = StatesLog::find()->andWhere(['user_id' => $this->user->id, 'states' => StatesLog::CATEGORY_LIST])->orderBy(['id' => SORT_DESC])->one();
                $activity = $this->command = $state->command;
                $params = $state->params;
                $logIds = StatesLog::find()->select('id')->andWhere(['user_id' => $this->user->id])->andWhere(['>=', 'id', $state->id])->column();
                StatesLog::deleteAll(['id' => $logIds]);
                break;
            case StatesLog::SEND_LOCATION:
                $skip = true;
                $state = StatesLog::find()->andWhere(['user_id' => $this->user->id, 'states' => StatesLog::CATEGORY_LIST])->orderBy(['id' => SORT_DESC])->one();
                $logIds = StatesLog::find()->select('id')->andWhere(['user_id' => $this->user->id])->andWhere(['>=', 'id', $state->id])->column();
                StatesLog::deleteAll(['id' => $logIds]);
                break;
        }

        $state = StatesLog::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->offset(1)->one();
        if ($state && !$skip) {
            if ($state->states == StatesLog::FOOD_QUANTITY) {
                $state = StatesLog::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->offset(3)->one();
                if ($state) {
                    $activity = $this->command = $state->command;
                    $params = $state->params;
                    $logIds = StatesLog::find()->select('id')->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(4)->offset(0)->column();
                    StatesLog::deleteAll(['id' => $logIds]);
                }
            } elseif ($state->states == StatesLog::ATTRIBUTE_LIST) {
                $state = StatesLog::find()->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(1)->offset(2)->one();
                if ($state) {
                    $activity = $this->command = $state->command;
                    $params = $state->params;
                    $logIds = StatesLog::find()->select('id')->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(3)->offset(0)->column();
                    StatesLog::deleteAll(['id' => $logIds]);
                }
            } else {
                $activity = $this->command = $state->command;
                $params = $state->params;
                $logIds = StatesLog::find()->select('id')->andWhere(['user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->limit(2)->offset(0)->column();
                StatesLog::deleteAll(['id' => $logIds]);
            }
        }
        $params ? $this->$activity($params) : $this->$activity();
    }

// params is array of id and distance
    public
    function SendCategories($params)
    {
        // id of entity saved in params field
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::CATEGORY_LIST, $this->message->message_id, $this->command, $params);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Выберите категорию') . " \u{2B07}";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [];
        $categories = FoodLink::find()->select("c.title")->distinct()->from(FoodLink::tableName() . ' fl')
            ->innerJoin(Entity::tableName() . ' e', "fl.entity_id = e.id")
            ->innerJoin(Category::tableName() . ' c', "fl.category_id = c.id")
            ->where(['fl.entity_id' => $params])->asArray()->all();

        foreach ($categories as $category) {
            $keyboardCount = $keyboard->getKeyboard();
            $button = new KeyboardButton();
            $button->text = "\u{2705} " . $category['title'];
            if (count($keyboardCount) == 0) {
                $keyboard->addButton($button);
                continue;
            }
            if (count(end($keyboardCount)) == 1) {
                $keyboard->addButton($button, count($keyboardCount), 2);
            } else {
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
            }
        }

        $button = new KeyboardButton();
        $button->text = "\u{1F6D2} " . Yii::t('main', 'Корзина');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{1F696} " . Yii::t('main', 'Условия доставки');
        $keyboard->addButton($button, count($keyboard->keyboard), 2);

        $button = new KeyboardButton();
        $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();

        $entity = Entity::findOne($params);
        $location = explode(',', $this->user->location);
        $curl = new Curl();
        $response = $curl->get("https://maps.googleapis.com/maps/api/directions/json?origin={$entity->latitude},{$entity->longitude}&destination={$location[0]},{$location[1]}&mode=driving&units=metric&alternatives=false&key=AIzaSyAzYOL6bw06c3P1Tq3aZoXH34RviHjyAro");
        $this->user->map_info = $response;
        $result = json_decode($response);
        $this->user->location_distance = number_format($result->routes[0]->legs[0]->distance->value / 1000, 2);
        $this->user->location_text = $result->routes[0]->legs[0]->end_address;
        $this->user->save(false);
    }

    public
    function SendEmptyCategories($params = null)
    {
        // id of entity saved in params field
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::CATEGORY_LIST, $this->message->message_id, $this->command, $params);

        $log = $state::find()->andWhere(['user_id' => $this->user->id, 'states' => StatesLog::SEND_LOCATION])->orderBy(['id' => SORT_DESC])->one();
        $entityDomain = EntityDomain::findOne($log->params);
        if ($entityDomain) {
            $request = $this->api->sendMessage();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', "В пределах 12 км учреждение {domain} не обслуживает на вашей территории!", ['domain' => $entityDomain->name]);

            $keyboard = new ReplyKeyboardMarkup();
            $keyboard->keyboard = [];

            $button = new KeyboardButton();
            $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
            $keyboard->addButton($button, 1, 1);

            $button = new KeyboardButton();
            $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
            $keyboard->addButton($button, 2, 1);

            $request->reply_markup = $keyboard;
            $request->send();
        }
    }

    public
    function SendLocation($params = null)
    {
        // entity domain ID saved in params field
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::SEND_LOCATION, $this->message->message_id, $this->command, $params);

        // delete all items in basket
        if (Basket::deleteAll(['user_id' => $this->user->id, 'status' => Basket::STATUS_NEW])) {
            $request = $this->api->sendMessage();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', 'Ваша корзина очищена') . "\u{2757}";
            $request->send();
        }

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Укажите Ваше местоположение ') . "\u{263A}";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4CC} " . Yii::t('main', 'Отправить местоположение'), 'request_location' => true]],
            [['text' => "\u{2B05} " . Yii::t('main', 'Назад')]],
//            [['text' => "\u{23EE} " . Yii::t('main', 'Главное меню')]],
        ];

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public
    function SendFoods($params, $msgType = 'Select')
    {
        // category ID saved in params field
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::FOOD_LIST, $this->message->message_id, $this->command, $params);

        TempBasket::deleteAll(['user_id' => $this->user->id]);

        $entityLog = StatesLog::find()->andWhere(['states' => StatesLog::CATEGORY_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;

        if ($msgType == 'Select')
            $request->text = Yii::t('main', 'Выберите товар') . " \u{2B07}";
        else
            $request->text = Yii::t('main', 'Продолжим?') . " \u{1F609}";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [];
        $time = new DateTime('now');
//        $today = $time->format('Y-m-d');
//        $subQuery = (new \yii\db\Query)
//            ->select([new \yii\db\Expression('1')])
//            ->from('rejected_food_log l')
//            ->where('f.id = l.food_id')
//            ->andWhere(['l.entity_id' => $entityLog->params])
//            ->andWhere(['>=', 'l.created_at', $today . ' 00:00:00']);

        $foods = Food::find()->from(Food::tableName() . ' f')
            ->innerJoin(FoodLink::tableName() . ' fl', 'f.id=fl.food_id')
            ->andWhere(['fl.entity_id' => $entityLog->params, 'fl.category_id' => $params, 'fl.active' => true])
//            ->andWhere(['not exists', $subQuery])
            ->orderBy(['id' => SORT_DESC])->all();

        $button = new KeyboardButton();
        $button->text = "\u{1F6D2} " . Yii::t('main', 'Корзина');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        foreach ($foods as $food) {
            $keyboardCount = $keyboard->getKeyboard();
            $button = new KeyboardButton();
            $button->text = "\u{1F924} " . $food['title'];
            if (count($keyboardCount) == 1) {
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                continue;
            }
            if (count(end($keyboardCount)) == 1) {
                $keyboard->addButton($button, count($keyboardCount), 2);
            } else {
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
            }
        }


        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{1F696} " . Yii::t('main', 'Условия доставки');
        $keyboard->addButton($button, count($keyboard->keyboard), 2);

        $button = new KeyboardButton();
        $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();

    }

//    public function SendFood($params)
//    {
//        // params is food object
//        $request = $this->api->sendPhoto();
//        $request->chat_id = $this->user->telegram_id;
//        $request->photo = Photos::findOne($params->image_id)->file_id;
//        $request->parse_mode = 'HTML';
//        $request->caption = Yii::t('main', $params->description);
//
//        $state = StatesLog::findOne(['user_id' => $this->user->id, 'states' => StatesLog::CATEGORY_LIST]);
//        $foodLink = FoodLink::findOne(['entity_id' => $state->params, 'category_id' => $this->userState->params, 'food_id' => $params->id]);
//        $request->caption .= '<b> = ' . $foodLink->price . ' UZS</b>';
//
//        $keyboard = new InlineKeyboardMarkup();
//        for ($i = 1; $i < 6; $i++) {
//            $button = new InlineKeyboardButton();
//            $button->text = $i;
//            $button->callback_data = 'AddFood_' . $params->id . '_' . $i;
//            $keyboard->addButton($button, 1, $i);
//        }
//
//        $button = new InlineKeyboardButton();
//        $button->text = "\u{1F6D2} " . Yii::t('main', 'Корзина');
//        $button->callback_data = 'Basket';
//        $keyboard->addButton($button, 2, 1);
//        $request->reply_markup = $keyboard;
//
//        $request->send();
//    }


    public
    function SendFood($command = null)
    {
        // $command is food title
        $state = StatesLog::find()->andWhere(['user_id' => $this->user->id, 'states' => StatesLog::CATEGORY_LIST])->orderBy(['id' => SORT_DESC])->one();

        $params = Food::find()->from(Food::tableName() . ' f')
            ->innerJoin(FoodLink::tableName() . ' fl', 'f.id=fl.food_id')
            ->andWhere(['fl.entity_id' => $state->params, 'fl.category_id' => $this->userState->params])
            ->andWhere(['f.title' => $command])
            ->orderBy(['id' => SORT_DESC])->one();

        $request = $this->api->sendPhoto();
        $request->chat_id = $this->user->telegram_id;
        $request->photo = Photos::findOne($params->image_id)->file_id;
        $request->parse_mode = 'HTML';

        $descriptions = explode('&', $params->description);
        foreach ($descriptions as $description) {
            $request->caption .= Yii::t('main', $description) . PHP_EOL;
        }

        $foodLink = FoodLink::findOne(['entity_id' => $state->params, 'category_id' => $this->userState->params, 'food_id' => $params->id]);

        $request->caption .= '<b>' . $foodLink->price . ' UZS</b>';


        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [];

        $sql = "
            SELECT * FROM attributes a 
            INNER JOIN attributes_link al on a.id = al.attribute_id
            WHERE not exists (
                SELECT * FROM temp_basket
                WHERE user_id = {$this->user->id} AND food_id = {$params->id} 
                AND entity_id = {$state->params} AND group_id = al.group_id
            ) AND al.food_id = {$params->id} AND al.entity_id = {$state->params}
            ORDER BY al.group_id
        ";

        $db = Yii::$app->db;
        $query = $db->createCommand($sql);
        $result = $query->queryAll();

        if ($result) {
            // food id saved
            $state = new StatesLog();
            $state->log($this->user->id, StatesLog::ATTRIBUTE_LIST, $this->message->message_id, $this->command, $params->id);
            $currentGroup = null;
            foreach ($result as $attribute) {
                $keyboardCount = $keyboard->getKeyboard();
                if ($currentGroup == null)
                    $currentGroup = $attribute['group_id'];
                if ($currentGroup && $currentGroup != $attribute['group_id'])
                    break;
                $button = new KeyboardButton();
//                $button->text = "\u{1F924} " . $attribute['title'];
                $button->text = $attribute['title'];
                if (count($keyboardCount) == 0) {
                    $keyboard->addButton($button);
                    continue;
                }
                if (count(end($keyboardCount)) == 1) {
                    $keyboard->addButton($button, count($keyboardCount), 2);
                } else {
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                }
            }
        } else {
            // food id and word no_attributes saved
            $state = new StatesLog();
            $state->log($this->user->id, StatesLog::FOOD_QUANTITY, $this->message->message_id, $this->command, $params->id . '-' . 'no_attributes');
            for ($i = 1; $i < 10; $i++) {
                $keyboardCount = $keyboard->getKeyboard();
                $button = new KeyboardButton();
                $button->text = $i;
                if (count($keyboardCount) == 0) {
                    $keyboard->addButton($button);
                    continue;
                }
                if (count(end($keyboardCount)) == 1) {
                    $keyboard->addButton($button, count($keyboardCount), 2);
                } elseif (count(end($keyboardCount)) == 2) {
                    $keyboard->addButton($button, count($keyboardCount), 3);
                } else {
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                }
            }
        }

        $button = new KeyboardButton();
        $button->text = "\u{1F6D2} " . Yii::t('main', 'Корзина');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{1F696} " . Yii::t('main', 'Условия доставки');
        $keyboard->addButton($button, count($keyboard->keyboard), 2);

        $button = new KeyboardButton();
        $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public
    function SendAttributes()
    {

        $db = Yii::$app->db;
        $state = StatesLog::find()->andWhere(['user_id' => $this->user->id, 'states' => StatesLog::CATEGORY_LIST])->orderBy(['id' => SORT_DESC])->one();
        $stateCategory = StatesLog::find()->andWhere(['states' => StatesLog::FOOD_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        $params = Food::findOne($this->userState->params);
        $sql = "
            SELECT a.id, al.group_id FROM attributes a 
            INNER JOIN attributes_link al on a.id = al.attribute_id
            WHERE al.food_id = {$params->id} AND al.entity_id = {$state->params} AND a.title = '{$this->command}'
        ";
        $query = $db->createCommand($sql);
        $result = $query->queryOne();

        if ($result) {
            try {
                TempBasket::log($this->user->id, $params->id, $result['id'], $state->params, $stateCategory->params, $result['group_id']);
            } catch (\Exception $e) {
                $log = new ErrorsLog();
                $log->message = $e->getMessage();
                $log->save(false);
            }
        }

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [];

        $sql = "
            SELECT * FROM attributes a 
            INNER JOIN attributes_link al on a.id = al.attribute_id
            WHERE not exists (
                SELECT * FROM temp_basket
                WHERE user_id = {$this->user->id} AND food_id = {$params->id} 
                AND entity_id = {$state->params} AND group_id = al.group_id
            ) AND al.food_id = {$params->id} AND al.entity_id = {$state->params}
            ORDER BY al.group_id
        ";


        $query = $db->createCommand($sql);
        $result = $query->queryAll();

        if ($result) {
            $currentGroup = null;
            foreach ($result as $attribute) {
                $keyboardCount = $keyboard->getKeyboard();
                if ($currentGroup == null)
                    $currentGroup = $attribute['group_id'];
                if ($currentGroup && $currentGroup != $attribute['group_id'])
                    break;
                $button = new KeyboardButton();
                $button->text = $attribute['title'];
                if (count($keyboardCount) == 0) {
                    $keyboard->addButton($button);
                    continue;
                }
                if (count(end($keyboardCount)) == 1) {
                    $keyboard->addButton($button, count($keyboardCount), 2);
                } else {
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                }
            }

            $group = AttributesGroup::findOne($currentGroup);
            $request->text = Yii::t('main', 'Пожалуйста, выберите ') . Yii::t('main', $group->title);

        } else {
            // food id and word no_attributes saved
            $state = new StatesLog();
            $state->log($this->user->id, StatesLog::FOOD_QUANTITY, $this->message->message_id, $this->command, $params->id . '-' . 'has_attributes');
            for ($i = 1; $i < 10; $i++) {
                $keyboardCount = $keyboard->getKeyboard();
                $button = new KeyboardButton();
                $button->text = $i;
                if (count($keyboardCount) == 0) {
                    $keyboard->addButton($button);
                    continue;
                }
                if (count(end($keyboardCount)) == 1) {
                    $keyboard->addButton($button, count($keyboardCount), 2);
                } elseif (count(end($keyboardCount)) == 2) {
                    $keyboard->addButton($button, count($keyboardCount), 3);
                } else {
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                }
            }
            $request->text = Yii::t('main', 'Пожалуйста, выберите количество');
        }

        $button = new KeyboardButton();
        $button->text = "\u{1F6D2} " . Yii::t('main', 'Корзина');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{2B05} " . Yii::t('main', 'Назад');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $button = new KeyboardButton();
        $button->text = "\u{1F696} " . Yii::t('main', 'Условия доставки');
        $keyboard->addButton($button, count($keyboard->keyboard), 2);

        $button = new KeyboardButton();
        $button->text = "\u{23EE} " . Yii::t('main', 'Главное меню');
        $keyboard->addButton($button, count($keyboard->keyboard) + 1, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public
    function AddBasket()
    {
        $stateEntity = StatesLog::find()->andWhere(['states' => StatesLog::CATEGORY_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        $stateCategory = StatesLog::find()->andWhere(['states' => StatesLog::FOOD_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        $stateFood = StatesLog::find()->andWhere(['states' => StatesLog::FOOD_QUANTITY, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();

        $stateFood = explode('-', $stateFood->params);

        if ($stateFood[1] == 'no_attributes') {
            if($this->command > 0) {
                $basket = new Basket();
                $basket->user_id = $this->user->id;
                $basket->food_id = $stateFood[0];
                $basket->entity_id = $stateEntity->params;
                $basket->category_id = $stateCategory->params;
                $basket->amount = $this->command;
                $basket->status = Basket::STATUS_NEW;
                $basket->save(false);
            }
        } else {
            $tempBaskets = TempBasket::find()
                ->select('attribute_id')
                ->andWhere(['user_id' => $this->user->id, 'food_id' => $stateFood[0], 'entity_id' => $stateEntity->params])
                ->orderBy('attribute_id')
                ->column();

            $combination = implode('-', $tempBaskets);

            if($this->command > 0) {
                $basket = new Basket();
                $basket->user_id = $this->user->id;
                $basket->food_id = $stateFood[0];
                $basket->entity_id = $stateEntity->params;
                $basket->category_id = $stateCategory->params;
                $basket->amount = $this->command;
                $basket->status = Basket::STATUS_NEW;
                $basket->combination = $combination;
                $basket->save(false);
            }
        }
        $this->SendFoods($stateCategory->params, 'Continue');
    }

    public
    function Basket()
    {
        $db = Yii::$app->db;
        $basketStatus = Basket::STATUS_NEW;
        $sql = "
        SELECT b.id as basket_id, e.id as entity_id, b.combination, b.food_id, e.name as entity, c.title as category, f.title as food, SUM(b.amount*fl.price) as price, SUM(b.amount) as amount, fl.price as cost FROM basket b
        INNER JOIN entity e on e.id = b.entity_id
        INNER JOIN category c ON c.id = b.category_id
        INNER JOIN food f ON f.id = b.food_id
        INNER JOIN food_link fl ON fl.entity_id = b.entity_id AND fl.category_id = b.category_id AND fl.food_id=b.food_id 
        WHERE user_id = {$this->user->id} AND b.status = '{$basketStatus}'
        GROUP BY f.title, b.combination, b.food_id
        ORDER BY c.id
        ";
        $request = $db->createCommand($sql);
        $orders = $request->queryAll();

//        var_dump($orders); die;

        $result = '';
        $entity = '';
        $count = 0;
        $keyboard = new InlineKeyboardMarkup();
        $button = new InlineKeyboardButton();
        $button->text = "\u{2705} " . Yii::t('main', 'Оформить заказ');
        $button->callback_data = 'SendPaymentTypes';
        $keyboard->addButton($button, 1, 1);

        foreach ($orders as $order) {
            if (!$entity)
                $entity = Yii::t('main', 'Название заведения:') . " <b>{$order['entity']}</b>" . PHP_EOL;
            $attrLabel = '';
            if ($order['combination']) {
                $exploded = explode('-', $order['combination']);
                $combination = implode('', $exploded);
                $attrPrice = AttributesPrice::findOne([
                    'combination' => $combination,
                    'food_id' => $order['food_id'],
                    'entity_id' => $order['entity_id']]);
                if ($attrPrice) {
                    $order['cost'] = $attrPrice->price;
                    $order['price'] = $attrPrice->price * $order['amount'];

                    if (($attrs = Attributes::findAll(['id' => $exploded])) != null) {
                        $attrLabel .= '<code>(';
                        $counter = 1;
                        foreach ($attrs as $attr) {
                            if ($counter == 1)
                                $attrLabel .= $attr->title;
                            else
                                $attrLabel .= ' + ' . $attr->title;
                            $counter++;
                        }
                        $attrLabel .= ')</code>';
                    }
                }
            }
            $count += intval($order['price']);
            $order['price'] = number_format($order['price'], 0, ',', ' ');
            $order['cost'] = number_format($order['cost'], 0, ',', ' ');

            $keyboardCount = $keyboard->getInlineKeyboard();

            if ($attrLabel) {
                $result .= "{$order['food']}:\n{$attrLabel}\n<b>{$order['amount']} x {$order['cost']} UZS = {$order['price']} UZS</b>" . PHP_EOL;

                $attrLabel = str_replace('<code>', ' ', $attrLabel);
                $attrLabel = str_replace('</code>', ' ', $attrLabel);
                $button = new InlineKeyboardButton();
                $button->text = "\u{274C} {$order['food']}:{$attrLabel}";
                $button->callback_data = 'EmptyBasket_' . $order['basket_id'];
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);

            } else {
                $result .= "{$order['food']}: \n<b>{$order['amount']} x {$order['cost']} UZS = {$order['price']} UZS</b>" . PHP_EOL;
                $button = new InlineKeyboardButton();
                $button->text = "\u{274C} {$order['food']}";
                $button->callback_data = 'EmptyBasket_' . $order['basket_id'];
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);
            }
        }
        if ($count > 0) {
            $result = $entity . PHP_EOL . $result . PHP_EOL;
            $deliveryPrice = GeneralHelper::calculateDelivery($this->user->location_distance);
            $totalPrice = $count + $deliveryPrice;
            $deliveryPrice = number_format($deliveryPrice, 0, ',', ' ');
            $result .= Yii::t('main', 'Доставка:') . "  {$this->user->location_distance}km - <b>{$deliveryPrice} UZS (нал.)</b>" . PHP_EOL;
            $count = number_format($count, 0, ',', ' ');
            $result .= Yii::t('main', 'Заказ:') . "  <b>{$count} UZS</b>" . PHP_EOL;
            $totalPrice = number_format($totalPrice, 0, ',', ' ');
            $result .= Yii::t('main', 'Общая сумма:') . "  <b>{$totalPrice} UZS</b>";
        }

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->parse_mode = 'html';
        $request->text = $result ? $result : Yii::t('main', 'Ваша корзина пуста!');

        if ($result) {
            $keyboardCount = $keyboard->getInlineKeyboard();
            $button = new InlineKeyboardButton();
            $button->text = "\u{274C} " . Yii::t('main', 'Полная очистка корзины');
            $button->callback_data = 'EmptyBasket_All';
            $keyboard->addButton($button, count($keyboardCount) + 1, 1);

            $request->reply_markup = $keyboard;
        }

        $request->send();
    }

    public
    function ConditionDelivery()
    {
        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->parse_mode = 'html';
        $request->text = Yii::t('main', 'Тарификация: минимальная стоимость <b>7000 UZS</b> за первые <b>3 км</b>');
        $request->text .= Yii::t('main', '— далее 1300 UZS/км.');
        $request->send();
    }

    public
    function changeLanguage($params)
    {
        $langs = [
            "\u{1F1FA}\u{1F1FF}  O'zbek" => "uz",
            "\u{1F1F7}\u{1F1FA}  Русский" => "ru",
        ];

        $this->user->lang = $langs[$params];
        $this->user->save(false);
        Yii::$app->language = $this->user->lang;
        $this->command = 'afterPhoneSent';
        $this->afterPhoneSent();
    }

    public
    function Settings()
    {
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::SETTINGS, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста, выберите раздел') . " \u{2B07}";
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F30D} " . Yii::t('main', 'Изменить язык')]],
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Изменить номер телефона')]],
            [['text' => "\u{2B05} " . Yii::t('main', 'Назад')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public
    function changeLang()
    {
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::CHANGE_LANG, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста, выберите язык') . " \u{2B07}";
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F1FA}\u{1F1FF}  O'zbek"]],
            [['text' => "\u{1F1F7}\u{1F1FA}  Русский"]],
            [['text' => "\u{2B05} " . Yii::t('main', 'Назад')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public
    function changePhoneSettings()
    {
        $state = new StatesLog();
        $state->log($this->user->id, StatesLog::PHONE_APPROVE_SETTINGS, $this->message->message_id, $this->command);

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Пожалуйста отправьте свой мобильный номер чтобы связаться с вами!') . PHP_EOL;
        $request->text .= Yii::t('main', 'Если у вас есть другой номер для связи, просто отправьте нам в виде: <b>998911234567</b>') . PHP_EOL;
        $request->text .= Yii::t('main', 'Затем мы вышлем вам <b>код</b> для подтверждения!');
        $request->parse_mode = "HTML";

        $keyboard = new ReplyKeyboardMarkup();
        $keyboard->keyboard = [
            [['text' => "\u{1F4DE} " . Yii::t('main', 'Отправить номер телефона'), 'request_contact' => true]],
            [['text' => "\u{2B05} " . Yii::t('main', 'Назад')]],
        ];
        $request->reply_markup = $keyboard;
        $request->send();
    }
}