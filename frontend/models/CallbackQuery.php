<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/12/2018
 * Time: 10:55 PM
 */

namespace frontend\models;


use api\keyboard\button\InlineKeyboardButton;
use api\keyboard\InlineKeyboardMarkup;
use api\keyboard\ReplyKeyboardMarkup;
use common\helpers\GeneralHelper;
use common\helpers\SendPushNotification;
use common\helpers\SendSMSHelper;
use common\models\Attributes;
use common\models\AttributesPrice;
use common\models\Basket;
use common\models\Devices;
use common\models\Entity;
use common\models\ErrorsLog;
use common\models\FoodLink;
use common\models\Orders;
use common\models\Photos;
use common\models\Rate;
use common\models\StatesLog;
use common\models\UserRoleLink;
use Yii;

class CallbackQuery
{
    public $user;
    public $message;
    public $api;

    public function __construct()
    {
        $this->api = Yii::$app->botClient;
    }

    // $data is array ['method', 'food_id', 'amount']
    public function AddFood($data)
    {
        $stateEntity = StatesLog::find()->andWhere(['states' => StatesLog::CATEGORY_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();
        $stateCategory = StatesLog::find()->andWhere(['states' => StatesLog::FOOD_LIST, 'user_id' => $this->user->id])->orderBy(['id' => SORT_DESC])->one();

        if (FoodLink::findOne(['entity_id' => $stateEntity->params, 'category_id' => $stateCategory->params, 'food_id' => $data[1]])) {
            $basket = new Basket();
            $basket->user_id = $this->user->id;
            $basket->food_id = $data[1];
            $basket->entity_id = $stateEntity->params;
            $basket->category_id = $stateCategory->params;
            $basket->amount = $data[2];
            $basket->status = Basket::STATUS_NEW;
            $basket->save(false);

            $request = $this->api->answerCallbackQuery();
            $request->callback_query_id = $this->message->id;
            $request->text = Yii::t('main', 'Добавлено в корзину!');
            $request->send();
        } else {
            $request = $this->api->answerCallbackQuery();
            $request->callback_query_id = $this->message->id;
            $request->text = Yii::t('main', 'Предыдущие сообщения становятся неактивными после выбора другой еды!');
            $request->send();
        }
    }

    public function Order($data = null)
    {

        $db = Yii::$app->db;
        $basketStatus = Basket::STATUS_NEW;
        $sql = "
        SELECT b.combination, b.food_id, b.entity_id, b.amount,  SUM(b.amount*fl.price) as totalPrice FROM basket b
        INNER JOIN food_link fl ON fl.entity_id = b.entity_id AND fl.category_id = b.category_id AND fl.food_id=b.food_id 
        WHERE user_id = {$this->user->id} AND b.status = '{$basketStatus}'
        GROUP BY b.combination, b.food_id, b.entity_id, b.amount
        ";
        $request = $db->createCommand($sql);
        $orders = $request->queryAll();
        if ($orders) {
            $entity_id = null;
            $totalPrice = 0;
            foreach ($orders as $order) {
                $entity_id = $order['entity_id'];

                if ($order['combination']) {
                    $exploded = explode('-', $order['combination']);
                    $combination = implode('', $exploded);
                    $attrPrice = AttributesPrice::findOne([
                        'combination' => $combination,
                        'food_id' => $order['food_id'],
                        'entity_id' => $order['entity_id']]);
                    if ($attrPrice) {
                        $totalPrice += $attrPrice->price * $order['amount'];
                    }
                } else
                    $totalPrice += $order['totalPrice'];
            }
            $entity = Entity::find()->andWhere(['id' => $entity_id, 'active' => true])->andWhere("(CURRENT_TIME() BETWEEN CAST(works_from AS time) AND CAST(works_to AS time)
        OR (NOT CURRENT_TIME() BETWEEN CAST(works_to AS time) AND CAST(works_from AS time) AND CAST(works_from AS time) >= CAST(works_to AS time)))")->one();
            if($entity == null){
                $request = $this->api->editMessageText();
                $request->chat_id = $this->user->telegram_id;
                $request->text = Yii::t('main', 'Учреждение временно не принимает заказы!')  . " \u{1F60A}";

                $request->parse_mode = 'HTML';

                $request->message_id = $this->message->message->message_id;
                $keyboard = new InlineKeyboardMarkup();
                $keyboard->inline_keyboard = [];
                $request->reply_markup = $keyboard;
                $request->send();
                return;
            }
            $order = new Orders();
            $order->user_id = $this->user->id;
            $order->entity_id = $entity_id;
            $order->totalPrice = $totalPrice;
            $order->status = Orders::STATUS_NEW;
            $order->confirm_code = rand(1000, 10000);
            $order->order_type = $data[1];
            $order->payment_type = $data[2];
            $order->user_location = $this->user->location;
            $order->user_location_distance = $this->user->location_distance;
            $order->user_location_text = $this->user->location_text;
            $order->map_info = $this->user->map_info;
            $order->save(false);

            $basket = Basket::findAll(['user_id' => $this->user->id, 'status' => Basket::STATUS_NEW]);
            foreach ($basket as $item) {
                $item->order_id = $order->id;
                $item->status = Basket::STATUS_ORDERED;
                $item->save(false);
            }


            $request = $this->api->answerCallbackQuery();
            $request->callback_query_id = $this->message->id;
            $request->text = Yii::t('main', 'Ваш заказ принят на обработку!');
            $request->send();

            $request = $this->api->editMessageText();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', 'Ваш заказ принят на обработку!') . PHP_EOL;
            $request->text .= Yii::t('main', '<b>Номер заказа: {id}</b>', ['id' => $order->id]) . PHP_EOL;
            $request->text .= Yii::t('main', 'Пожалуйста подождите, пока менеджер {entityName} примет ваш заказ.', ['entityName' => Entity::findOne($order->entity_id)->name]) . PHP_EOL;

            if ($order->payment_type != Orders::PAY_CASH)
                $request->text .= Yii::t('main', '<b>Затем мы вышлем вам счет для оплаты.</b>');

            $request->parse_mode = 'HTML';
            $request->message_id = $this->message->message->message_id;
            $keyboard = new InlineKeyboardMarkup();
            $keyboard->inline_keyboard = [];
            $request->reply_markup = $keyboard;
            $request->send();


            GeneralHelper::sendPush($basket, $order, 'new');
            SendSMSHelper::sendSMS('998974494493', Yii::t('main', 'Azizbek shoshiling! Yangi zakaz bor {code} delivera.uz', ['code' => $order->id]));

//            sleep(2);
//
//            $request = $this->api->sendPhoto();
//            $request->chat_id = $this->user->telegram_id;
//            $request->photo = @Photos::findOne(['for_command' => 'afterPhoneSent'])->file_id;
//            $request->caption = Yii::t('main', 'Поделитесь контактом голодного друга' . " \u{1F60B}\u{1F60B}");
//            $request->send();

//            $state = new StatesLog();
//            $state->log($this->user->id, StatesLog::MAIN_MENU, $this->message->message->message_id, 'afterPhoneSent');
//
//            $request = $this->api->sendMessage();
//            $request->chat_id = $this->user->telegram_id;
//            $request->text = '';
//
//            $keyboard = new ReplyKeyboardMarkup();
//            $keyboard->keyboard = [
//                [['text' => "\u{1F37D} " . Yii::t('main', 'Заказать')]],
//                [['text' => "\u{1F6D2} " . Yii::t('main', 'Корзина')]],
//                [['text' => "\u{1F696} " . Yii::t('main', 'Условия доставки')]],
//                [['text' => "\u{2699} " . Yii::t('main', 'Настройки')]],
//            ];
//            $request->reply_markup = $keyboard;
//            $request->send();
        } else {
            $request = $this->api->editMessageText();
            $request->chat_id = $this->user->telegram_id;
            $request->text = Yii::t('main', 'Ваш заказ не может быть обработан. Ваша корзина пуста!') . PHP_EOL;

            $request->parse_mode = 'HTML';

            $request->message_id = $this->message->message->message_id;
            $keyboard = new InlineKeyboardMarkup();
            $keyboard->inline_keyboard = [];
            $request->reply_markup = $keyboard;
            $request->send();
        }
    }

    public function SendOrderTypes($data = null)
    {
        $request = $this->api->editMessageReplyMarkup();
        $request->chat_id = $this->user->telegram_id;
        $request->message_id = $this->message->message->message_id;

        $keyboard = new InlineKeyboardMarkup();

        $button = new InlineKeyboardButton();
        $button->text = Yii::t('main', 'Доставка');
        $button->callback_data = 'Order_' . Orders::TYPE_DELIVERY . '_' . $data[1];
        $keyboard->addButton($button, 1, 1);

        $button = new InlineKeyboardButton();
        $button->text = Yii::t('main', 'Самовывоз');
        $button->callback_data = 'Order_' . Orders::TYPE_SELF . '_' . $data[1];
        $keyboard->addButton($button, 2, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function SendPaymentTypes()
    {
        $request = $this->api->editMessageReplyMarkup();
        $request->chat_id = $this->user->telegram_id;
        $request->message_id = $this->message->message->message_id;

        $keyboard = new InlineKeyboardMarkup();

        $button = new InlineKeyboardButton();
        $button->text = "\u{1F4B5} " . Yii::t('main', 'Наличными');
//        $button->callback_data = 'SendOrderTypes_' . Orders::PAY_CASH;
        $button->callback_data = 'Order_' . Orders::TYPE_DELIVERY . '_' . Orders::PAY_CASH;
        $keyboard->addButton($button, 1, 1);

//        $button = new InlineKeyboardButton();
//        $button->text = "\u{1F4B3} " . Yii::t('main', 'PAYME');
//        $button->callback_data = 'Order_' . Orders::TYPE_DELIVERY . '_' . Orders::PAY_PAYME;
////        $button->callback_data = 'SendOrderTypes_' . Orders::PAY_PAYME;
//        $keyboard->addButton($button, 2, 1);

        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function EmptyBasket($data = null)
    {
        if (isset($data[1]) && $data[1] != 'All') {
            Basket::deleteAll(['id' => $data[1]]);

            $db = Yii::$app->db;
            $basketStatus = Basket::STATUS_NEW;
            $sql = "
        SELECT b.id as basket_id, e.id as entity_id, b.combination, b.food_id, e.name as entity, c.title as category, f.title as food, SUM(b.amount*fl.price) as price, SUM(b.amount) as amount, fl.price as cost FROM basket b
        INNER JOIN entity e on e.id = b.entity_id
        INNER JOIN category c ON c.id = b.category_id
        INNER JOIN food f ON f.id = b.food_id
        INNER JOIN food_link fl ON fl.entity_id = b.entity_id AND fl.category_id = b.category_id AND fl.food_id=b.food_id 
        WHERE user_id = {$this->user->id} AND b.status = '{$basketStatus}'
        GROUP BY f.title, b.combination, b.food_id
        ORDER BY c.id
        ";
            $request = $db->createCommand($sql);
            $orders = $request->queryAll();

            $result = '';
            $entity = '';
            $count = 0;
            $keyboard = new InlineKeyboardMarkup();
            $button = new InlineKeyboardButton();
            $button->text = "\u{2705} " . Yii::t('main', 'Оформить заказ');
            $button->callback_data = 'SendPaymentTypes';
            $keyboard->addButton($button, 1, 1);

            foreach ($orders as $order) {
                if (!$entity)
                    $entity = Yii::t('main', 'Название заведения:') . " <b>{$order['entity']}</b>" . PHP_EOL;
                $attrLabel = '';
                if ($order['combination']) {
                    $exploded = explode('-', $order['combination']);
                    $combination = implode('', $exploded);
                    $attrPrice = AttributesPrice::findOne([
                        'combination' => $combination,
                        'food_id' => $order['food_id'],
                        'entity_id' => $order['entity_id']]);
                    if ($attrPrice) {
                        $order['cost'] = $attrPrice->price;
                        $order['price'] = $attrPrice->price * $order['amount'];

                        if (($attrs = Attributes::findAll(['id' => $exploded])) != null) {
                            $attrLabel .= '<code>(';
                            $counter = 1;
                            foreach ($attrs as $attr) {
                                if ($counter == 1)
                                    $attrLabel .= $attr->title;
                                else
                                    $attrLabel .= ' + ' . $attr->title;
                                $counter++;
                            }
                            $attrLabel .= ')</code>';
                        }
                    }
                }
                $count += intval($order['price']);
                $order['price'] = number_format($order['price'], 0, ',', ' ');
                $order['cost'] = number_format($order['cost'], 0, ',', ' ');

                $keyboardCount = $keyboard->getInlineKeyboard();
                $log = new ErrorsLog();
                $log->message = $attrLabel;
                $log->save(false);
                if ($attrLabel) {
                    $result .= "{$order['food']}:\n{$attrLabel}\n<b>{$order['amount']} x {$order['cost']} UZS = {$order['price']} UZS</b>" . PHP_EOL;

                    $attrLabel = str_replace('<code>', ' ', $attrLabel);
                    $attrLabel = str_replace('</code>', ' ', $attrLabel);
                    $button = new InlineKeyboardButton();
                    $button->text = "\u{274C} {$order['food']}:{$attrLabel}";
                    $button->callback_data = 'EmptyBasket_' . $order['basket_id'];
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);

                } else {
                    $result .= "{$order['food']}: \n<b>{$order['amount']} x {$order['cost']} UZS = {$order['price']} UZS</b>" . PHP_EOL;
                    $button = new InlineKeyboardButton();
                    $button->text = "\u{274C} {$order['food']}";
                    $button->callback_data = 'EmptyBasket_' . $order['basket_id'];
                    $keyboard->addButton($button, count($keyboardCount) + 1, 1);
                }
            }
            if ($count > 0) {
                $result = $entity . PHP_EOL . $result . PHP_EOL;
                $deliveryPrice = GeneralHelper::calculateDelivery($this->user->location_distance);
                $totalPrice = $count + $deliveryPrice;
                $deliveryPrice = number_format($deliveryPrice, 0, ',', ' ');
                $result .= Yii::t('main', 'Доставка:') . "  {$this->user->location_distance}km - <b>{$deliveryPrice} UZS (нал.)</b>" . PHP_EOL;
                $count = number_format($count, 0, ',', ' ');
                $result .= Yii::t('main', 'Заказ:') . "  <b>{$count} UZS</b>" . PHP_EOL;
                $totalPrice = number_format($totalPrice, 0, ',', ' ');
                $result .= Yii::t('main', 'Общая сумма:') . "  <b>{$totalPrice} UZS</b>";
            }

            $request = $this->api->editMessageText();
            $request->chat_id = $this->user->telegram_id;
            $request->message_id = $this->message->message->message_id;
            $request->parse_mode = 'html';
            $request->text = $result ? $result : Yii::t('main', 'Ваша корзина пуста!');

            if ($result) {
                $keyboardCount = $keyboard->getInlineKeyboard();
                $button = new InlineKeyboardButton();
                $button->text = "\u{274C} " . Yii::t('main', 'Полная очистка корзины');
                $button->callback_data = 'EmptyBasket_All';
                $keyboard->addButton($button, count($keyboardCount) + 1, 1);

                $request->reply_markup = $keyboard;
            }

            $request->send();

        } else {
            if (Basket::deleteAll(['user_id' => $this->user->id, 'status' => Basket::STATUS_NEW])) {
                $request = $this->api->editMessageText();
                $request->chat_id = $this->user->telegram_id;
                $request->text = Yii::t('main', 'Ваша корзина очищена') . "\u{2757}";
                $request->parse_mode = 'HTML';
                $request->message_id = $this->message->message->message_id;
                $keyboard = new InlineKeyboardMarkup();
                $keyboard->inline_keyboard = [];
                $request->reply_markup = $keyboard;
                $request->send();
            }
        }
    }

    public function Rate($data)
    {
        $rate = new Rate();
        $rate->user_id = $this->user->id;
        $rate->mark = $data[1];
        $rate->save(false);

        $request = $this->api->answerCallbackQuery();
        $request->callback_query_id = $this->message->id;
        $request->text = Yii::t('main', 'Спасибо за вашу оценку!');
        $request->send();

        $request = $this->api->editMessageText();
        $request->chat_id = $this->user->telegram_id;
        $request->text = Yii::t('main', 'Приятного аппетита') . " \u{1F60B}";
        $request->parse_mode = 'HTML';
        $request->message_id = $this->message->message->message_id;
        $keyboard = new InlineKeyboardMarkup();
        $keyboard->inline_keyboard = [];
        $request->reply_markup = $keyboard;
        $request->send();
    }

    public function Basket($data = null)
    {
        $request = $this->api->answerCallbackQuery();
        $request->callback_query_id = $this->message->id;
        $request->send();
        $db = Yii::$app->db;
        $basketStatus = Basket::STATUS_NEW;
        $sql = "
        SELECT e.name as entity, c.title as category, f.title as food, SUM(b.amount*fl.price) as price, SUM(b.amount) as amount, fl.price as cost FROM basket b
        INNER JOIN entity e on e.id = b.entity_id
        INNER JOIN category c ON c.id = b.category_id
        INNER JOIN food f ON f.id = b.food_id
        INNER JOIN food_link fl ON fl.entity_id = b.entity_id AND fl.category_id = b.category_id AND fl.food_id=b.food_id 
        WHERE user_id = {$this->user->id} AND b.status = '{$basketStatus}'
        GROUP BY f.title
        ORDER BY c.id
        ";
        $request = $db->createCommand($sql);
        $orders = $request->queryAll();

        $result = '';
        $entity = '';
        $count = 0;
        foreach ($orders as $order) {
            if (!$entity)
                $entity = Yii::t('main', 'Учреждение обработки ваш заказ:') . " \n<b>{$order['entity']}</b>" . PHP_EOL;
            $result .= "{$order['food']}:\n<b>{$order['amount']} x {$order['cost']} UZS = {$order['price']} UZS</b>" . PHP_EOL;
            $count += $order['price'];
        }
        if ($count > 0) {
            $result = $entity . PHP_EOL . $result . PHP_EOL;
            $deliveryPrice = GeneralHelper::calculateDelivery($this->user->location_distance);
            $result .= Yii::t('main', 'Доставка:') . "  {$this->user->location_distance} km - <b>{$deliveryPrice} UZS</b>" . PHP_EOL;
            $result .= Yii::t('main', 'Итого:') . "  <b>{$count} UZS</b>";
        }

        $request = $this->api->sendMessage();
        $request->chat_id = $this->user->telegram_id;
        $request->parse_mode = 'html';
        $request->text = $result ? $result : Yii::t('main', 'Ваша корзина пуста!');

        if ($result) {
            $keyboard = new InlineKeyboardMarkup();

            $button = new InlineKeyboardButton();
            $button->text = "\u{2705} " . Yii::t('main', 'Оформить заказ');
            $button->callback_data = 'SendPaymentTypes';
            $keyboard->addButton($button, 1, 1);

            $button = new InlineKeyboardButton();
            $button->text = "\u{274C} " . Yii::t('main', 'Полная очистка корзины');
            $button->callback_data = 'EmptyBasket';
            $keyboard->addButton($button, 2, 1);

            $request->reply_markup = $keyboard;
        }

        $request->send();
    }

}