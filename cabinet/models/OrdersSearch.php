<?php

namespace cabinet\models;

use common\models\Attributes;
use common\models\Basket;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form of `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $removedItems = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'entity_id', 'driver_id', 'confirm_code'], 'integer'],
            [['totalPrice', 'status', 'period', 'created_at', 'updated_at', 'order_type'], 'safe'],
            [['removedItems'], 'required', 'message' => Yii::t('main', 'Выберите хотя бы одну еду')],
            [['period'], 'required', 'message' => Yii::t('main', 'Поле, обязательное для заполнения')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here
        $query->andWhere(['entity_id' => Yii::$app->session->get('current_entity'), 'status' => [self::STATUS_NEW, self::STATUS_PAID]]);
        $query->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'entity_id' => $this->entity_id,
            'driver_id' => $this->driver_id,
            'confirm_code' => $this->confirm_code,
        ]);

        $query->andFilterWhere(['like', 'totalPrice', $this->totalPrice])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'order_type', $this->order_type]);

        return $dataProvider;
    }

    public function searchMyOrders($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here
        $query->andWhere(['entity_id' => Yii::$app->session->get('current_entity')]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'entity_id' => $this->entity_id,
            'driver_id' => $this->driver_id,
            'confirm_code' => $this->confirm_code,
        ]);

        $query->andFilterWhere(['like', 'totalPrice', $this->totalPrice])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'order_type', $this->order_type]);

        return $dataProvider;
    }

    public function getBasket()
    {
        $basket = Basket::findAll(['order_id' => $this->id]);
        $items = [];
        foreach ($basket as $item) {
            $itemAttributes = [];
            if ($item->combination) {
                $exploded = explode('-', $item->combination);
                if (($attrs = Attributes::findAll(['id' => $exploded])) != null) {
                    foreach ($attrs as $attr) {
                        $itemAttributes[] = $attr->title;
                    }
                }
            }
            if ($itemAttributes)
                $items[$item->id] = $item->food->title . '(' . implode(';', $itemAttributes) . ')';
            else
                $items[$item->id] = $item->food->title;
        }
        return $items;
    }

    public function getTimes()
    {
        return [
            10 => Yii::t('main', '10 минут'),
            15 => Yii::t('main', '15 минут'),
            20 => Yii::t('main', '20 минут'),
            25 => Yii::t('main', '25 минут'),
            30 => Yii::t('main', '30 минут'),
        ];
    }
}
