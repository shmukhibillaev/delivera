<?php

namespace cabinet\controllers;

use api\response\LabeledPrice;
use common\helpers\GeneralHelper;
use common\models\Basket;
use common\models\BotUsers;
use common\models\FoodLink;
use common\models\Invoice;
use common\models\Photos;
use common\models\RejectedFoodLog;
use Faker\Provider\Base;
use Yii;
use common\models\Orders;
use cabinet\models\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionReject($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate(['removedItems'])) {
            $model->status = $model::STATUS_REJECTED;
            $model->save(false);
            GeneralHelper::rejectOrder($model);
            return $this->redirect(['index']);
        }


        return $this->render('reject', [
            'model' => $model,
        ]);
    }

    public function actionAcceptToProcess($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate(['period'])) {
            $model->status = $model::STATUS_IN_PROCESS;
            $model->save(false);
            GeneralHelper::sendOrderTime($model);
//            $basket = Basket::findAll([['order_id' => $model->id]]);
            GeneralHelper::sendPushDriver($model->id, 'paid');
            return $this->redirect(['index']);
        }
        return $this->render('accept', [
            'model' => $model,
        ]);
    }

    public function actionAcceptToApprove($id)
    {
        $model = $this->findModel($id);
        GeneralHelper::sendInvoice($model);
        $model->status = $model::STATUS_APPROVED;
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdersSearch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }

    public function actionMyOrders()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->searchMyOrders(Yii::$app->request->queryParams);

        return $this->render('my-orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
