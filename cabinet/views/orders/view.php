<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">

    <h1>Номер заказа:  <?= Html::encode($this->title) ?></h1>

    <h3>Данные пользователя:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->last_name . ' ' . $model->user->first_name;
                },
                'label' => 'Ф.И.О пользователя'
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->phone;
                },
                'label' => 'Телефон номер пользователя'
            ]
        ],
    ]) ?>
    <p></p>
    <h3>Данные заказа:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'created_at',
                'label' => 'Дата заказа'
            ],
            [
                'value' => function ($model) {
                    return $model->getFoods();
                },
                'label' => 'Список еды',
                'format' => 'raw'
            ],
            [
                'attribute' => 'totalPrice',
                'value' => function ($model) {
                    return $model->totalPrice . ' UZS';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatuses()[$model->status];
                }
            ],
            [
                'attribute' => 'payment_type',
                'value' => function ($model) {
                    return $model->getPaymentTypes()[$model->payment_type];
                },
                'label' => 'Тип оплаты',
            ],
//            'period',
        ],
    ]) ?>
</div>
