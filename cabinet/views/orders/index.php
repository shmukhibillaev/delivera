<?php

use common\models\Orders;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel cabinet\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Orders */

$this->title = Yii::t('main', 'Заказы');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile("/cabinet/web/js/push.js", [View::POS_HEAD]);
$this->registerJs(' 
  Push.Permission.request(onGranted, onDenied);
   function onGranted() {
    }

    function onDenied() {
    }
 ', \yii\web\VIEW::POS_END);
$this->registerJs(' 
    setInterval(function(){  
         $.pjax.reload({container:"#pjaxContainer"});
    }, 10000);', \yii\web\VIEW::POS_HEAD);

$bodyText = Yii::t('main', 'Пожалуйста подтвердите заказ!');
$titleText = Yii::t('main', 'Новый заказ!');
$logo = Yii::getAlias("/cabinet/web/favicon.ico");
$url = Yii::$app->request->absoluteUrl;
$this->registerJs("
$('#pjaxContainer').on('pjax:end', function() {
        if(rowCount > 0){
            Push.create('{$titleText}', {
        body: '{$bodyText}',
        icon: '{$logo}',
        timeout: 5000,
        onClick :function(){
            window.focus();
            this.close();
        },
    });
        }
       });
", $this::POS_READY);
?>

<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(['id' => 'pjaxContainer']) ?>
    <?php
    $this->registerJs("
        rowCount  = {$dataProvider->getTotalCount()}
    ;
", $this::POS_READY);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => ['style' => 'font-size:20px'],
        'options' => [
            'style'=>'word-wrap: break-word;'
        ],
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'totalPrice',
            [
                'label' => Yii::t('main', 'Заказанные Блюда'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getFoods();
                }
            ],
//            [
//                'attribute' => 'order_type',
//                'value' => function ($model) {
//                    return $model->getDeliveryTypes()[$model->order_type];
//                }
//            ],
            [
                'attribute' => 'payment_type',
                'value' => function ($model) {
                    return $model->getPaymentTypes()[$model->payment_type];
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatuses()[$model->status];
                }
            ],

            [
                'attribute' => 'entity_id',
                'label' => 'Название заведения',
                'value' => function ($model) {
                    return $model->entity->name;
                },
                'visible' => Yii::$app->session->get('current_role_id') == \common\models\UserRoleLink::OPERATOR
            ],

            [
                'label' => Yii::t('main', 'Действия'),
                'format' => 'raw',
                'value' => function ($model) {
                    $btn = '';
                    if ($model->status == Orders::STATUS_NEW && $model->payment_type != Orders::PAY_CASH) {
                        $btn .= Html::a(Yii::t('main', 'Принять'), ['orders/accept-to-approve', 'id' => $model->id], ['class' => 'text-primary', 'style' => 'font-size:20px']);
                        $btn .= Html::a(Yii::t('main', 'Отказать'), ['orders/reject', 'id' => $model->id], [
                            'class' => 'text-danger',
                            'style' => 'font-size:20px;  padding-left:10px;',
//                            'data' => [
//                                'confirm' => Yii::t('main', 'Вы уверены, что хотите отменить заказ?'),
//                                'method' => 'post',
//                            ],
                        ]);
                    }
                    if($model->status == Orders::STATUS_PAID && $model->payment_type != Orders::PAY_CASH){
                        $btn .= Html::a(Yii::t('main', 'Подтвердить'), ['orders/accept-to-process', 'id' => $model->id], ['class' => 'text-success', 'style' => 'font-size:20px;  padding-left:10px']);
                    }

                    if($model->status == Orders::STATUS_NEW && $model->payment_type == Orders::PAY_CASH){
                        $btn .= Html::a(Yii::t('main', 'Принять'), ['orders/accept-to-process', 'id' => $model->id], ['class' => 'text-success', 'style' => 'font-size:20px;  padding-left:10px']);
                        $btn .= Html::a(Yii::t('main', 'Отказать'), ['orders/reject', 'id' => $model->id], [
                            'class' => 'text-danger',
//                            'style' => 'font-size:30px; margin-left:5px;',
                            'style' => 'font-size:20px; padding-left:10px',
//                            'data' => [
//                                'confirm' => Yii::t('main', 'Вы уверены, что хотите отменить заказ?'),
//                                'method' => 'post',
//                            ],
                        ]);
                    }
                    $btn .= Html::a(Yii::t('main', 'Посмотреть'), ['orders/view', 'id' => $model->id], ['class' => 'text-info', 'style' => 'font-size:20px;  padding-left:10px']);
                    return $btn;
                }
            ]
            //'order_type',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
