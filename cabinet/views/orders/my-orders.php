<?php

use common\models\Orders;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel cabinet\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Orders */

$this->title = Yii::t('main', 'Заказы');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(['id' => 'pjaxContainer']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'totalPrice',
            [
                'label' => Yii::t('main', 'Заказанные Блюда'),
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getFoods();
                }
            ],
//            [
//                'attribute' => 'order_type',
//                'value' => function ($model) {
//                    return $model->getDeliveryTypes()[$model->order_type];
//                }
//            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatuses()[$model->status];
                }
            ],
            [
                'attribute' => 'entity_id',
                'label' => 'Название заведения',
                'value' => function ($model) {
                    return $model->entity->name;
                },
                'visible' => Yii::$app->session->get('current_role_id') == \common\models\UserRoleLink::OPERATOR
            ],
//            [
//                'label' => Yii::t('main', 'Действия'),
//                'format' => 'raw',
//                'value' => function ($model) {
//                    $btn = '';
//                    if ($model->status == Orders::STATUS_NEW) {
//                        $btn .= Html::a(Yii::t('main', 'Принять'), ['orders/accept', 'id' => $model->id], ['class' => 'btn btn-primary', 'style' => 'font-size:30px']);
//                        $btn .= Html::a(Yii::t('main', 'Отказать'), ['orders/reject', 'id' => $model->id], [
//                            'class' => 'btn btn-danger',
//                            'style' => 'font-size:30px; margin-left:5px;',
////                            'data' => [
////                                'confirm' => Yii::t('main', 'Вы уверены, что хотите отменить заказ?'),
////                                'method' => 'post',
////                            ],
//                        ]);
//                    }
//                    if($model->status == Orders::STATUS_PAID){
//                        $btn .= Html::a(Yii::t('main', 'Подтвердить'), ['orders/accept-to-process', 'id' => $model->id], ['class' => 'btn btn-success', 'style' => 'font-size:30px']);
//                    }
//                    return $btn;
//                }
//            ]
            //'order_type',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
