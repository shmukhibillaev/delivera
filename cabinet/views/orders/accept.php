<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/23/2018
 * Time: 1:31 PM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('main', 'Отказать');


?>
<style>
    input {
        min-width:35px;
        min-height:35px
    }
    </style>
<div class="orders-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'period')->radioList($model->getTimes(), [
        'style' => 'display:grid !important; font-size:35px;',
        'required' => true
    ])
        ->label(Yii::t('main', 'Сколько времени требуется для обработки заказа?'), ['style' => 'font-size:35px'])

    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Принять'), ['class' => 'btn btn-success', 'style' => 'font-size:35px']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

