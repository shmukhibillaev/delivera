<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/23/2018
 * Time: 1:31 PM
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Заказы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('main', 'Отказать');


?>
<style>
    input {
        min-width:35px;
        min-height:35px
    }
    </style>
<div class="orders-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'removedItems')->checkboxList($model->getBasket(), [
        'style' => 'display:grid !important; font-size:35px;',
    ])
        ->label(Yii::t('main', 'Выберите еду, которой сейчас нет'), ['style' => 'font-size:35px']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Отказать'), ['class' => 'btn btn-success', 'style' => 'font-size:35px']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

