<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost:3306;dbname=delivera',
            'username' => 'delivera',
            'password' => '3qHC88gQi1dTCe870TGb',
            'charset' => 'utf8mb4',
            'on afterOpen' => function($event) {
                $event->sender->createCommand("SET time_zone='Asia/Tashkent';")->execute();
            },
        ],
    ],
];
