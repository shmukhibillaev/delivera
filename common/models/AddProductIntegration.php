<?php


namespace common\models;


use yii\base\Model;

/**
 * @property string $title
 * @property int $integration_food_id
 * @property int $category_id
 * @property int $price
 * @property int $image_id
 * @property boolean $is_active
 */
class AddProductIntegration extends Model
{
    public $integration_food_id;
    public $title;
    public $category_id;
    public $price;
    public $entity_id;
    public $image_id;
    public $is_active;

    public function rules()
    {
        return [
            [['title'], 'string'],
            [['is_active'], 'boolean'],
            [['integration_food_id', 'price', 'image_id', 'category_id'], 'integer'],
            [['title', 'integration_food_id', 'category_id', 'price', 'image_id', 'is_active'], 'required'],
        ];
    }

    public function add()
    {
        $this->mapCategoryAndImage();
        if ($this->validate()) {
            $food = Food::findOne(['integration_food_id' => $this->entity_id . '-' . $this->integration_food_id]);
            if (!$food)
                $food = new Food();

            $food->integration_food_id = $this->entity_id . '-' . $this->integration_food_id;
            $food->title = $this->title;
            $food->image_id = (string)$this->image_id;
            if ($food->save()) {
                $foodLink = FoodLink::findOne(['entity_id' => $this->entity_id, 'food_id' => $food->id, 'category_id' => $this->category_id]);
                if (!$foodLink)
                    $foodLink = new FoodLink();
                $foodLink->entity_id = $this->entity_id;
                $foodLink->food_id = $food->id;
                $foodLink->category_id = $this->category_id;
                $foodLink->price = $this->price;
                $foodLink->active = $this->is_active;
                if ($foodLink->save())
                    return ['status' => true, 'message' => 'Successfully Saved'];
                else
                    return ['status' => false, 'message' => $foodLink->getErrors()];
            } else {
                return ['status' => false, 'message' => $food->getErrors()];
            }
        }
        return ['status' => false, 'message' => $this->getErrors()];
    }

    public function mapCategoryAndImage()
    {
        // Category Map for  Krepost
        if (in_array($this->entity_id, [133])) {
            $categories = [
                1 => 280,
                2 => 281,
                3 => 274,
                4 => 282,
                5 => 278,
                6 => 275,
            ];
            $this->category_id = @$categories[$this->category_id];

            $categoriesImages = [
                280 => 6112,
                281 => 6113,
                274 => 6114,
                282 => 6115,
                278 => 6116,
                275 => 6117,
            ];
            $this->image_id = @$categoriesImages[$this->category_id];
            return;
        }
        $this->category_id = null;
        $this->image_id = null;
    }
}