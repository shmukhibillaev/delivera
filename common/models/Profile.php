<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property int $user_id
 * @property int $field_id
 * @property string $value
 */
class Profile extends \yii\db\ActiveRecord
{
    const FIELD_ENTITY = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'field_id', 'value'], 'required'],
            [['user_id', 'field_id'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'field_id' => Yii::t('main', 'Field ID'),
            'value' => Yii::t('main', 'Value'),
        ];
    }

    public static function Add($user_id, $field_id, $value){
        $model = new self();
        $model->user_id = $user_id;
        $model->field_id = $field_id;
        $model->value = $value;
        $model->save(false);
    }
}
