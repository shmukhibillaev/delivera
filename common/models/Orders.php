<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property int $entity_id
 * @property string $totalPrice
 * @property string $status
 * @property string $period
 * @property int $driver_id
 * @property int $confirm_code
 * @property string $created_at
 * @property string $updated_at
 * @property string $order_type
 * @property string $payment_type
 * @property string $user_location
 * @property string $user_location_distance
 * @property string $user_location_text
 * @property string $map_info
 */
class Orders extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_APPROVED = 'approved';
    const STATUS_PAID = 'paid';
    const STATUS_REJECTED = 'rejected';
    const STATUS_IN_PROCESS = 'in_process';
    const STATUS_ON_WAY = 'on_way';
    const STATUS_PROCESSED = 'processed';

    const TYPE_SELF = 'self';
    const TYPE_DELIVERY = 'delivery';

    const PAY_CASH = 'cash';
    const PAY_PAYME = 'payme';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'entity_id', 'totalPrice', 'status', 'period', 'driver_id', 'confirm_code', 'created_at', 'updated_at', 'order_type'], 'required'],
            [['user_id', 'entity_id', 'driver_id', 'confirm_code'], 'integer'],
            [['totalPrice', 'status', 'period', 'created_at', 'updated_at', 'order_type', 'payment_type'], 'string', 'max' => 255],
            [['user_location', 'user_location_distance', 'user_location_text', 'map_info',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'Номер заказа'),
            'user_id' => Yii::t('main', 'User ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'totalPrice' => Yii::t('main', 'Общая сумма (UZS)'),
            'status' => Yii::t('main', 'Статус'),
            'period' => Yii::t('main', 'Period'),
            'driver_id' => Yii::t('main', 'Driver ID'),
            'confirm_code' => Yii::t('main', 'Confirm Code'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'order_type' => Yii::t('main', 'Тип заказа'),
        ];
    }

    public function getStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('main', 'Новый'),
            self::STATUS_APPROVED => Yii::t('main', 'Принят'),
            self::STATUS_PAID => Yii::t('main', 'Оплачено'),
            self::STATUS_REJECTED => Yii::t('main', 'Отказано'),
            self::STATUS_IN_PROCESS => Yii::t('main', 'В обработке'),
            self::STATUS_PROCESSED => Yii::t('main', 'Обработано'),
            self::STATUS_ON_WAY => Yii::t('main', 'В пути'),
        ];
    }

    public function getFoods()
    {
        $foods = Basket::find()->select('food_id, combination, sum(amount) as amount')->andWhere(['order_id' => $this->id])->with('food')->groupBy('food_id, combination')->all();
        $result = '';
        foreach ($foods as $food) {
            $itemAttributes = [];
            if ($food->combination) {
                $exploded = explode('-', $food->combination);
                if (($attrs = Attributes::findAll(['id' => $exploded])) != null) {
                    foreach ($attrs as $attr) {
                        $itemAttributes[] = $attr->title;
                    }
                }
            }
            if ($itemAttributes)
                $result .= $food->food->title . '(' . implode(';', $itemAttributes) . '): <span style="color: red;">' . $food->amount . '</span></br>';
            else
                $result .= $food->food->title . ': <span style="color: red;">' . $food->amount . '</span></br>';
        }
        return $result;
    }

    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    public function getUser()
    {
        return $this->hasOne(BotUsers::className(), ['id' => 'user_id']);
    }

    public function getDriver()
    {
        return $this->hasOne(Drivers::className(), ['id' => 'driver_id']);
    }

    public function getDeliveryTypes()
    {
        return [
            self::TYPE_DELIVERY => Yii::t('main', 'Доставка'),
            self::TYPE_SELF => Yii::t('main', 'Самовывоз'),
        ];
    }

    public function getPaymentTypes()
    {
        return [
            self::PAY_CASH => Yii::t('main', 'Наличными'),
            self::PAY_PAYME => Yii::t('main', 'PAYME'),
        ];
    }
}
