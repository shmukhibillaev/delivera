<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rejected_food_log".
 *
 * @property int $id
 * @property int $entity_id
 * @property int $food_id
 * @property string $created_at
 */
class RejectedFoodLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rejected_food_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'food_id'], 'required'],
            [['entity_id', 'food_id'], 'integer'],
            [['created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'food_id' => Yii::t('main', 'Food ID'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }

    public static function Log($entity_id, $food_id){
        $model = new self();
        $model->entity_id = $entity_id;
        $model->food_id = $food_id;
        $model->save(false);
    }
}
