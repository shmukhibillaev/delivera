<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "region_list".
 *
 * @property int $id
 * @property string $title
 * @property int $parent_id
 * @property int $soato_id
 * @property int $depth
 * @property int $available
 */
class RegionList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'region_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'parent_id', 'soato_id', 'depth', 'available'], 'required'],
            [['parent_id', 'soato_id', 'depth', 'available'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
            'parent_id' => Yii::t('main', 'Parent ID'),
            'soato_id' => Yii::t('main', 'Soato ID'),
            'depth' => Yii::t('main', 'Depth'),
            'available' => Yii::t('main', 'Available'),
        ];
    }
}
