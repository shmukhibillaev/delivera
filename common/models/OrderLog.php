<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_log".
 *
 * @property int $id
 * @property int $order_id
 * @property string $prev_status
 * @property string $cur_status
 * @property string $action
 * @property int $time
 * @property string $date
 * @property int $user_id
 * @property string $user_type
 */
class OrderLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'prev_status', 'cur_status', 'action', 'time', 'user_id'], 'required'],
            [['order_id', 'time', 'user_id'], 'integer'],
            [['prev_status', 'cur_status', 'action', 'date', 'user_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'order_id' => Yii::t('main', 'Order ID'),
            'prev_status' => Yii::t('main', 'Prev Status'),
            'cur_status' => Yii::t('main', 'Cur Status'),
            'action' => Yii::t('main', 'Action'),
            'time' => Yii::t('main', 'Time'),
            'date' => Yii::t('main', 'Date'),
            'user_id' => Yii::t('main', 'User ID'),
            'user_type' => Yii::t('main', 'User Type'),
        ];
    }
    /** @var $order Orders */
    public static function log($order, $curStatus, $action, $user_type){
        $log = new self();
        $log->order_id = $order->id;
        $log->prev_status = $order->status;
        $log->cur_status = $curStatus;
        $log->action = $action;
        $log->time = round((strtotime(date("Y-m-d H:i:s")) - strtotime($order->updated_at))/60, 0);
        $log->user_id = Yii::$app->user->id;
        $log->user_type = $user_type;
        $log->save(false);
    }
}
