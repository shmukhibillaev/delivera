<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "states_log".
 *
 * @property int $id
 * @property int $user_id
 * @property int $states
 * @property int $message_id
 * @property string $date
 * @property string $command
 * @property string $params
 */
class StatesLog extends \yii\db\ActiveRecord
{
    const PHONE_SEND = 1;
    const MAIN_MENU = 2;
    const ENTITY_LIST = 3;
    const SEND_LOCATION = 4;
    const CATEGORY_LIST = 5;
    const FOOD_LIST = 6;
    const CATEGORY_DOMAIN_LIST = 7;
    const PHONE_APPROVE = 8;
    const CODE_SEND = 9;
    const SETTINGS = 10;
    const CHANGE_LANG = 11;
    const SEND_FOOD = 12;
    const FOOD_QUANTITY = 13;
    const ATTRIBUTE_LIST = 14;
    const PHONE_APPROVE_SETTINGS = 15;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'states_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'states', 'message_id'], 'integer'],
            [['date'], 'safe'],
            [['command', 'params'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'states' => Yii::t('main', 'States'),
            'message_id' => Yii::t('main', 'Message ID'),
            'date' => Yii::t('main', 'Date'),
            'command' => Yii::t('main', 'Command'),
            'params' => Yii::t('main', 'Params'),
        ];
    }

    public function log($user, $state, $message_id, $command, $params = null){
        if($command == 'Back')
            return;
        $this->user_id = $user;
        $this->states = $state;
        $this->message_id = $message_id;
        $this->command = $command;
        $this->params = $params;
        $this->save(false);
    }
}
