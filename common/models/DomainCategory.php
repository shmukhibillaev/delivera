<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "domain_category".
 *
 * @property int $id
 * @property string $title
 * @property integer $order
 */
class DomainCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'domain_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['order'], 'integer'],
            [['order', 'title'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
        ];
    }
}
