<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rate".
 *
 * @property int $id
 * @property int $user_id
 * @property int $mark
 * @property string $comment
 * @property string $date
 */
class Rate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'mark'], 'required'],
            [['user_id', 'mark'], 'integer'],
            [['comment', 'date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'mark' => Yii::t('main', 'Mark'),
            'comment' => Yii::t('main', 'Comment'),
            'date' => Yii::t('main', 'Date'),
        ];
    }
}
