<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "confirm_codes".
 *
 * @property int $id
 * @property int $user_id
 * @property int $code
 * @property string $phone
 */
class ConfirmCodes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'confirm_codes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'code', 'phone'], 'required'],
            [['user_id', 'code'], 'integer'],
            [['phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'code' => Yii::t('main', 'Code'),
            'phone' => Yii::t('main', 'Phone'),
        ];
    }

    public static function log($user_id, $code, $phone){
        $model = new self();
        $model->user_id = $user_id;
        $model->code = $code;
        $model ->phone = $phone;
        $model->save(false);
    }
}
