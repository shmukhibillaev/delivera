<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property int $user_id
 * @property string $platform
 * @property string $app_version
 * @property string $device_uuid
 * @property string $device_info
 * @property string $access_token
 * @property int $is_active
 * @property string $created_at
 * @property string $last_action_at
 * @property string $ip
 * @property string $push_token
 * @property string $user_type
 */
class Devices extends \yii\db\ActiveRecord
{
    const DRIVERS = 'drivers';
    const ENTITY = 'entity';
    const CLIENT = 'client';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'is_active'], 'integer'],
            [['device_info', 'user_type'], 'string'],
            [['app_version'], 'safe'],
            [['platform', 'device_uuid', 'access_token', 'created_at', 'last_action_at', 'ip', 'push_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'platform' => Yii::t('main', 'Platform'),
            'app_version' => Yii::t('main', 'App Version'),
            'device_uuid' => Yii::t('main', 'Device Uuid'),
            'device_info' => Yii::t('main', 'Device Info'),
            'access_token' => Yii::t('main', 'Access Token'),
            'is_active' => Yii::t('main', 'Is Active'),
            'created_at' => Yii::t('main', 'Created At'),
            'last_action_at' => Yii::t('main', 'Last Action At'),
            'ip' => Yii::t('main', 'Ip'),
            'push_token' => Yii::t('main', 'Push Token'),
            'user_type' => Yii::t('main', 'User Type'),
        ];
    }

    public static function registerDevice($user, $params, $user_type)
    {
        $device = null;
        if ($params['device_uuid']) {
            $device = self::findOne(['device_uuid' => $params['device_uuid'], 'user_type' => $user_type]);
        } elseif ($params['push_token']) {
            $device = self::findOne(['push_token' => $params['push_token'], 'user_type' => $user_type]);
        }
        if (!$device) {
            $device = new self();
            $device->user_id = $user->id;
            $device->platform = @$params['platform'];
            $device->app_version = @$params['app_version'];
            $device->device_uuid = @$params['device_uuid'];
            $device->device_info = @$params['device_info'];
            $device->access_token = Yii::$app->security->generateRandomString(32);
            $device->ip = Yii::$app->request->userIP;
            $device->user_type = $user_type;
            $device->push_token = @$params['push_token'];

            if (!$device->save())
                ErrorsLog::log($device->getErrors(), 'mobile');
        }

        return $device;
    }
}
