<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_role_link".
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property int $entity_id
 * @property string $entity_ids
 */
class UserRoleLink extends \yii\db\ActiveRecord
{
    const ADMIN = 1;
    const MANAGER = 2;
    const HEAD = 3;
    const OPERATOR = 4;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_role_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id', 'entity_id'], 'required'],
            [['user_id', 'role_id', 'entity_id'], 'integer'],
            [['entity_ids'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'role_id' => Yii::t('main', 'Role ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'entity_ids' => Yii::t('main', 'Entity IDs'),
        ];
    }

    public static function Add($user_id, $role_id, $entity_id){
        $model = new self();
        $model->user_id = $user_id;
        $model->role_id = $role_id;
        $model->entity_id = $entity_id;
        $model->save(false);
    }

    public static function getRoles(){
        return [
            self::MANAGER => 'Manager',
            self::HEAD => 'HEAD'
        ];
    }

    public static function getRoleCode()
    {
        return [
            self::MANAGER => 'manager',
            self::HEAD => 'head'
        ];
    }

        public static function getUsernames(){
        return ArrayHelper::map(User::find()->all(), 'id', 'username');
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

}
