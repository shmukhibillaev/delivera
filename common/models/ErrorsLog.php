<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "errors_log".
 *
 * @property int $id
 * @property string $date
 * @property string $message
 */
class ErrorsLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'errors_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'date' => Yii::t('main', 'Date'),
            'message' => Yii::t('main', 'Message'),
        ];
    }

    public static function log($data, $type){
        $model = new self();
        $model->message = json_encode([
            'type' => $type,
            'data' => $data
        ]);
        $model->save(false);
    }
}
