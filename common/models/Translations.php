<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translations".
 *
 * @property int $id
 * @property string $key_word
 * @property string $uz
 * @property string $ru
 */
class Translations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key_word'], 'required'],
            [['key_word', 'uz', 'ru'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'key_word' => Yii::t('main', 'Key Word'),
            'uz' => Yii::t('main', 'Uz'),
            'ru' => Yii::t('main', 'Ru'),
        ];
    }
}
