<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "photos".
 *
 * @property int $id
 * @property string $title
 * @property string $real_path
 * @property string $file_id
 * @property string $for_command
 * @property string $url
 * @property string $aws_result
 * @property int $created_at
 * @property int $updated_at
 * @property int $user_id
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_id', 'url', 'aws_result'], 'string'],
            [['created_at', 'updated_at', 'user_id'], 'integer'],
            [['title', 'real_path', 'for_command'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
            'real_path' => Yii::t('main', 'Real Path'),
            'file_id' => Yii::t('main', 'File ID'),
            'for_command' => Yii::t('main', 'For Command'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
            'user_id' => Yii::t('main', 'User ID'),
        ];
    }

    public function getImageUrl(){
        return "https://delivera.uz/" . $this->real_path;
    }
}
