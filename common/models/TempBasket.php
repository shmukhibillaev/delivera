<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "temp_basket".
 *
 * @property int $id
 * @property int $food_id
 * @property int $attribute_id
 * @property int $user_id
 * @property int $entity_id
 * @property int $category_id
 * @property int $group_id
 * @property string $date
 */
class TempBasket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_basket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['food_id', 'attribute_id', 'user_id', 'entity_id', 'category_id'], 'required'],
            [['food_id', 'attribute_id', 'user_id', 'entity_id', 'category_id', 'group_id'], 'integer'],
            [['date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'food_id' => Yii::t('main', 'Food ID'),
            'attribute_id' => Yii::t('main', 'Attribute ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'category_id' => Yii::t('main', 'Category ID'),
            'date' => Yii::t('main', 'Date'),
        ];
    }

    public static function log($user_id, $food_id, $attribute_id , $entity_id, $category_id, $group_id){
        $model = new self();
        $model->user_id = $user_id;
        $model->food_id = $food_id;
        $model->attribute_id = $attribute_id;
        $model->entity_id = $entity_id;
        $model->category_id = $category_id;
        $model->group_id = $group_id;
        $model->save(false);
    }
}
