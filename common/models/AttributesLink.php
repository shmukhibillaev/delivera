<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attributes_link".
 *
 * @property int $id
 * @property int $group_id
 * @property int $attribute_id
 * @property int $food_id
 * @property int $entity_id
 * @property AttributesGroup $group
 * @property Attributes $attr
 */
class AttributesLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attributes_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'attribute_id', 'food_id', 'entity_id'], 'required'],
            [['group_id', 'attribute_id', 'food_id', 'entity_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'group_id' => Yii::t('main', 'Group ID'),
            'attribute_id' => Yii::t('main', 'Attribute ID'),
            'food_id' => Yii::t('main', 'Food ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
        ];
    }

    public function getGroup(){
        return $this->hasOne(AttributesGroup::className(), ['id' => 'group_id']);
    }

    public function getAttr(){
        return $this->hasOne(Attributes::className(), ['id' => 'attribute_id']);
    }
}
