<?php

namespace common\models;

use Foo;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\Photos;

/**
 * This is the model class for table "food_link".
 *
 * @property int $id
 * @property int $entity_id
 * @property int $category_id
 * @property int $food_id
 * @property double $price
 * @property boolean $active
 * @property Category $category
 * @property Entity $entity
 * @property Food $food
 */
class FoodLink extends \yii\db\ActiveRecord
{
    public $target_entity_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food_link';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'category_id', 'food_id'], 'integer'],
            [['price'], 'number'],
            [['active'], 'boolean'],
            [['target_entity_id'], 'safe'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['entity_id' => 'id']],
            [['food_id'], 'exist', 'skipOnError' => true, 'targetClass' => Food::className(), 'targetAttribute' => ['food_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'category_id' => Yii::t('main', 'Category ID'),
            'food_id' => Yii::t('main', 'Food ID'),
            'price' => Yii::t('main', 'Price'),
            'active' => Yii::t('main', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'entity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFood()
    {
        return $this->hasOne(Food::className(), ['id' => 'food_id']);
    }

    public function getCategories()
    {
        return ArrayHelper::map(Category::find()->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');
    }

    public function getEntities()
    {
        return ArrayHelper::map(Entity::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name');
    }

    public function getFoods()
    {
        $foods = ArrayHelper::map(Food::find()->all(), 'id', 'title');
//        foreach ($foods as $key => $value) {
//            $foods[$key] = Food::findOne($value)->title;
//        }
        return $foods;
    }

    public function Duplicate()
    {
        $models = self::findAll(['entity_id' => $this->entity_id]);
        foreach ($models as $model) {
            foreach ($this->target_entity_id as $target) {
                if ((self::findOne([
                        'entity_id' => $target,
                        'category_id' => $model->category_id,
                        'food_id' => $model->food_id
                    ])) == null) {
                    $new = new FoodLink();
                    $new->setAttributes($model->attributes);
                    $new->entity_id = $target;
                    $new->save();
                }
            }
        }
    }

    public function getAttributeLinks()
    {
        $attrs = AttributesLink::find()->andWhere(['entity_id' => $this->entity_id, 'food_id' => $this->food_id])->orderBy(['attribute_id' => SORT_ASC])->all();
        $result = [];
        foreach ($attrs as $attr) {
            $result[$attr->group_id][] = $attr->attribute_id;
        }
        $html = "";
        foreach ($result as $key => $value) {
            $html .= "<div class=\"panel panel-default\">";
            $groupTitle = AttributesGroup::findOne($key)->title;
            $html .= " <div class=\"panel-heading\">{$groupTitle}</div>";
            $innerContent = "";
            $attributes = Attributes::findAll($value);
            foreach ($attributes as $item){
                $innerContent .= $item->title . "<br />";
            }
            $html .= "<div class=\"panel-body\">{$innerContent}</div>";
            $html .= "</div>";
        }

        $attrPrices = AttributesPrice::findAll(['entity_id' => $this->entity_id, 'food_id' => $this->food_id]);
        $priceContent = "";
        foreach ($attrPrices as $attrPrice){
            if($attrPrice->attributes_data){
                $arr = json_decode($attrPrice->attributes_data);
                $attrs = Attributes::findAll($arr);
                $arrContent = [];
                foreach ($attrs as $attr){
                    $arrContent[] = $attr->title;
                }
                $priceContent .= implode(' + ', $arrContent) . ' = ' . $attrPrice->price . '<br />';
            }
        }
        if($priceContent){
            $html .= "
            <div class=\"panel panel-default\">
            <div class=\"panel-heading\">Attribute Prices</div>
            <div class=\"panel-body\">{$priceContent}</div>
            </div>
            ";
        }

        return $html;
    }
}
