<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "basket".
 *
 * @property int $id
 * @property int $user_id
 * @property int $entity_id
 * @property int $category_id
 * @property int $food_id
 * @property int $amount
 * @property int $order_id
 * @property string $date
 * @property string $status
 * @property string $combination
 */
class Basket extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_ORDERED = 'ordered';
    const STATUS_REJECTED = 'rejected';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'basket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'entity_id', 'category_id', 'food_id', 'amount', 'status'], 'required'],
            [['user_id', 'entity_id', 'category_id', 'food_id', 'amount', 'order_id'], 'integer'],
            [['date', 'status', 'combination'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'category_id' => Yii::t('main', 'Category ID'),
            'food_id' => Yii::t('main', 'Food ID'),
            'amount' => Yii::t('main', 'Amount'),
            'date' => Yii::t('main', 'Date'),
            'status' => Yii::t('main', 'Status'),
            'order_id' => Yii::t('main', 'Order ID'),
        ];
    }
    public function getFood()
    {
        return $this->hasOne(Food::className(), ['id' => 'food_id']);
    }
}
