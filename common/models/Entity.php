<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "entity".
 *
 * @property int $id
 * @property string $name
 * @property double $longitude
 * @property double $latitude
 * @property string $phone
 * @property string $responsible_person
 * @property int $entity_domain_id
 * @property int $region_id
 * @property int $district_id
 * @property string $works_from
 * @property string $works_to
 * @property boolean $active
 *
 * @property EntityDomain $entityDomain
 * @property FoodLink[] $foodLinks
 */
class Entity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'region_id', 'district_id'], 'required'],
            [['longitude', 'latitude'], 'string'],
            [['entity_domain_id', 'region_id', 'district_id'], 'integer'],
            [['active'], 'boolean'],
            [['name', 'phone', 'responsible_person', 'works_from', 'works_to'], 'string', 'max' => 255],
            [['entity_domain_id'], 'exist', 'skipOnError' => true, 'targetClass' => EntityDomain::className(), 'targetAttribute' => ['entity_domain_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'name' => Yii::t('main', 'Name'),
            'longitude' => Yii::t('main', 'Longitude'),
            'latitude' => Yii::t('main', 'Latitude'),
            'phone' => Yii::t('main', 'Phone'),
            'responsible_person' => Yii::t('main', 'Responsible Person'),
            'entity_domain_id' => Yii::t('main', 'Entity Domain ID'),
            'works_from' => Yii::t('main', 'Works From'),
            'works_to' => Yii::t('main', 'Works To'),
            'active' => Yii::t('main', 'Active'),
            'region_id' => Yii::t('main', 'Region'),
            'district_id' => Yii::t('main', 'District'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityDomain()
    {
        return $this->hasOne(EntityDomain::className(), ['id' => 'entity_domain_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodLinks()
    {
        return $this->hasMany(FoodLink::className(), ['entity_id' => 'id']);
    }

    public static function getEntityDomains(){
        return ArrayHelper::map(EntityDomain::find()->all(), 'id', 'name');
    }
}
