<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bot_users".
 *
 * @property int $id
 * @property int $telegram_id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $lang
 * @property string $phone
 * @property string $location
 * @property string $location_text
 * @property string $map_info
 * @property int $last_location
 * @property float $location_distance
 */
class BotUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bot_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telegram_id', 'last_location'], 'integer'],
            [['first_name', 'last_name', 'username', 'phone', 'location'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 10],
            [['location_distance', 'location_text', 'map_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'telegram_id' => Yii::t('main', 'Telegram ID'),
            'first_name' => Yii::t('main', 'First Name'),
            'last_name' => Yii::t('main', 'Last Name'),
            'username' => Yii::t('main', 'Username'),
            'lang' => Yii::t('main', 'Lang'),
            'phone' => Yii::t('main', 'Phone'),
            'location' => Yii::t('main', 'Location'),
            'last_location' => Yii::t('main', 'Last Location'),
            'location_distance' => Yii::t('main', 'Location Distance'),
        ];
    }
}
