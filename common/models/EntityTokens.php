<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "entity_tokens".
 *
 * @property int $id
 * @property int $entity_id
 * @property string $token
 */
class EntityTokens extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entity_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'token'], 'required'],
            [['entity_id'], 'integer'],
            [['token'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'token' => Yii::t('main', 'Token'),
        ];
    }
}
