<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pre_checkout".
 *
 * @property int $id
 * @property int $user_id
 * @property int $query_id
 * @property int $order_id
 * @property string $data
 * @property string $created_at
 */
class PreCheckout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pre_checkout';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'query_id', 'data', 'order_id'], 'required'],
            [['user_id', 'order_id'], 'integer'],
            [['data', 'query_id'], 'string'],
            [['created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'query_id' => Yii::t('main', 'Query ID'),
            'data' => Yii::t('main', 'Data'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }

    public static function log($user_id, $query_id, $data, $order_id){
        $model = new self();
        $model->user_id = $user_id;
        $model->query_id = $query_id;
        $model->data = $data;
        $model->order_id = $order_id;
        $model->save(false);
    }
}
