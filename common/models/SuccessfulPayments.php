<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "successful_payments".
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property string $provider_payment_charge_id
 * @property string $created_at
 * @property string $data
 */
class SuccessfulPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'successful_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'provider_payment_charge_id'], 'required'],
            [['user_id', 'order_id'], 'integer'],
            [['provider_payment_charge_id', 'created_at'], 'string', 'max' => 255],
            [['data'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'order_id' => Yii::t('main', 'Order ID'),
            'provider_payment_charge_id' => Yii::t('main', 'Provider Payment Charge ID'),
            'created_at' => Yii::t('main', 'Created At'),
        ];
    }
    public static function log($user_id, $order_id, $data, $charge_id){
        $model = new self();
        $model->user_id = $user_id;
        $model->order_id = $order_id;
        $model->data = $data;
        $model->provider_payment_charge_id = $charge_id;
        $model->save(false);
    }
}
