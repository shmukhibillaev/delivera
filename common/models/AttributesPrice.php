<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attributes_price".
 *
 * @property int $id
 * @property string $combination
 * @property int $food_id
 * @property int $entity_id
 * @property string $price
 * @property string $attributes_data
 */
class AttributesPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attributes_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['combination', 'food_id', 'entity_id', 'price'], 'required'],
            [['food_id', 'entity_id'], 'integer'],
            [['combination', 'price', 'attributes_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'combination' => Yii::t('main', 'Combination'),
            'food_id' => Yii::t('main', 'Food ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'price' => Yii::t('main', 'Price'),
            'attributes_data' => Yii::t('main', 'Attributes Data'),
        ];
    }
}
