<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property string $logo_unicode
 *
 * @property FoodLink[] $foodLinks
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'logo_unicode'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
            'logo_unicode' => Yii::t('main', 'Logo Unicode'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodLinks()
    {
        return $this->hasMany(FoodLink::className(), ['category_id' => 'id']);
    }
}
