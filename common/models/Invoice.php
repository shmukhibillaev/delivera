<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "invoice".
 *
 * @property int $id
 * @property int $user_id
 * @property int $entity_id
 * @property string $payload
 * @property string $start_parameter
 * @property string $provider_token
 * @property int $totalPrice
 * @property int $order_id
 * @property string $data
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class Invoice extends \yii\db\ActiveRecord
{
    const STATUS_WAIT = 'wait';
    const STATUS_PAID = 'paid';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'entity_id', 'totalPrice', 'created_at', 'updated_at', 'order_id'], 'required'],
            [['user_id', 'entity_id', 'totalPrice', 'order_id'], 'integer'],
            [['data'], 'string'],
            [['payload', 'start_parameter', 'provider_token', 'status', 'created_at', 'updated_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'user_id' => Yii::t('main', 'User ID'),
            'entity_id' => Yii::t('main', 'Entity ID'),
            'payload' => Yii::t('main', 'Payload'),
            'start_parameter' => Yii::t('main', 'Start Parameter'),
            'provider_token' => Yii::t('main', 'Provider Token'),
            'totalPrice' => Yii::t('main', 'Total Price'),
            'data' => Yii::t('main', 'Data'),
            'status' => Yii::t('main', 'Status'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public static function log($user_id, $entity_id, $payload, $start_parameter, $token, $totalPrice, $data, $order_id){
        $model = new self();
        $model->user_id = $user_id;
        $model->entity_id = $entity_id;
        $model->payload = $payload;
        $model->start_parameter = $start_parameter;
        $model->provider_token = $token;
        $model->totalPrice = $totalPrice;
        $model->data = $data;
        $model->status = self::STATUS_WAIT;
        $model->order_id = $order_id;
        $model->save(false);
    }
}
