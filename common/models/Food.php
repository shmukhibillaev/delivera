<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "food".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image_id
 * @property string $extra_info
 * @property string $integration_food_id
 *
 * @property FoodLink[] $foodLinks
 */
class Food extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'image_id', 'extra_info'], 'string'],
            [['title', 'integration_food_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('main', 'ID'),
            'title' => Yii::t('main', 'Title'),
            'description' => Yii::t('main', 'Description'),
            'image_id' => Yii::t('main', 'Image ID'),
            'extra_info' => Yii::t('main', 'Extra Info'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodLinks()
    {
        return $this->hasMany(FoodLink::className(), ['food_id' => 'id']);
    }

    public static function getImages(){
        return ArrayHelper::map(Photos::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'title');
    }

    public function getImage(){
        return $this->hasOne(Photos::className(), ['id' => 'image_id']);
    }
}
