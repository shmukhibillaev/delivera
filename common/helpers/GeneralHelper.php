<?php
/**
 * Created by PhpStorm.
 * User: s_muxibillayev
 * Date: 20.12.2018
 * Time: 9:34
 */

namespace common\helpers;


use api\keyboard\ReplyKeyboardMarkup;
use api\response\LabeledPrice;
use backend\models\DriversSearch;
use cabinet\models\OrdersSearch;
use common\models\Attributes;
use common\models\Basket;
use common\models\BotUsers;
use common\models\Devices;
use common\models\Drivers;
use common\models\ErrorsLog;
use common\models\FoodLink;
use common\models\Invoice;
use common\models\Orders;
use common\models\RejectedFoodLog;
use common\models\StatesLog;
use common\models\UserRoleLink;
use Yii;

class GeneralHelper
{
    public static function isJSON($string)
    {
        return (is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE));
    }

    public static function calculateDelivery($distance)
    {
//        if ($distance <= 3) {
//            return 7000;
//        } elseif ($distance <= 7) {
//            return 7000 + ($distance - 3) * 1250;
//        } else {
//            return 7000 + (7 - 3) * 1250 + ($distance - 7) * 1500;
//        }
        if ($distance <= 3) {
            return 7000;
        } else{
            return 7000 + ($distance - 3) * 1300;
        }
    }

    public static function sendInvoice($model)
    {
        $user = BotUsers::findOne($model->user_id);
        $api = Yii::$app->botClient;
        $request = $api->sendInvoice();
        $request->chat_id = $user->telegram_id;
        $request->title = $model->entity->name;
        $request->description = Yii::t('main', 'Ваш номер заказа: ') . $model->id . PHP_EOL;

        $basket = Basket::find()->andWhere(['order_id' => $model->id, 'status' => Basket::STATUS_ORDERED])->with('food')->all();
        $request->description .= '---' . PHP_EOL;
        foreach ($basket as $item) {
            $price = FoodLink::findOne(['entity_id' => $model->entity_id, 'food_id' => $item->food_id, 'category_id' => $item->category_id])->price;
            $request->description .= $item->food->title . ': ' . $item->amount . ' x ' . $price . ' = ' . $item->amount * $price . ' UZS' . PHP_EOL;
        }
        $request->description .= '---' . PHP_EOL;
        if ($model->order_type == Orders::TYPE_DELIVERY) {
            $deliveryPrice = GeneralHelper::calculateDelivery($user->location_distance);
            $request->description .= Yii::t('main', 'Доставка:') . "  {$user->location_distance} km - {$deliveryPrice} UZS" . PHP_EOL;
        } else {
            $request->description .= Yii::t('main', 'Самовывоз') . PHP_EOL;
        }

        $request->description .= Yii::t('main', 'Пожалуйста, оплатите счет для обработки вашего заказа!') . PHP_EOL;
        // assigns order id for further purposes
        $request->payload = $model->id;
        $request->start_parameter = Yii::$app->security->generateRandomString(32);
        $request->provider_token = "371317599:TEST:81799036";
        $request->currency = "UZS";

        $price = new LabeledPrice();
        $price->label = Yii::t('main', 'Общая сумма');
        $price->amount = $model->totalPrice * 100;
        $request->prices = [$price];

//        $request->is_flexible = true;
//        $request->photo_url = @Photos::findOne(['for_command' => 'afterPhoneSent'])->file_id;
        $data = $request->send();
        Invoice::log($model->user_id, $model->entity_id, $request->payload, $request->start_parameter, $request->provider_token, $model->totalPrice * 100, $data, $model->id);
    }

    public static function rejectOrder($model)
    {
        $basket = Basket::findAll(['id' => $model->removedItems, 'order_id' => $model->id]);
        $removedItems = '';
        foreach ($basket as $item) {
            $item->status = Basket::STATUS_REJECTED;
            $item->save(false);

            $itemAttributes = [];
            if ($item->combination) {
                $exploded = explode('-', $item->combination);
                if (($attrs = Attributes::findAll(['id' => $exploded])) != null) {
                    foreach ($attrs as $attr) {
                        $itemAttributes[] = $attr->title;
                    }
                }
            }
            if ($itemAttributes)
                $removedItems .= '<b>' . $item->food->title . '</b>' . ' <code>(' . implode(';', $itemAttributes) . ')</code>' . PHP_EOL;
            else
                $removedItems .= '<b>' . $item->food->title . '</b>' . PHP_EOL;
            RejectedFoodLog::Log($item->entity_id, $item->food_id);
        }

        $checkBasket = Basket::findAll(['order_id' => $model->id, 'status' => Basket::STATUS_ORDERED]);
        $api = Yii::$app->botClient;
        if ($checkBasket) {
            foreach ($checkBasket as $item) {
                $item->status = Basket::STATUS_NEW;
                $item->save(false);
            }
            $request = $api->sendMessage();
            $request->chat_id = BotUsers::findOne($model->user_id)->telegram_id;
            $request->parse_mode = 'HTML';
            $request->text = Yii::t('main', 'К сожалению, сегодня {entity} не предоставляет следующие продукты:', [
                    'entity' => $model->entity->name
                ]) . PHP_EOL;
            $request->text .= $removedItems;
            $request->text .= Yii::t('main', '<b>Эти продукты удалены из вашей корзины!</b>') . PHP_EOL;
            $request->text .= Yii::t('main', 'Может закажете что-то другое? ') . "\u{263A}";
            $request->send();
        } else {
            $request = $api->sendMessage();
            $request->chat_id = BotUsers::findOne($model->user_id)->telegram_id;
            $request->parse_mode = 'HTML';
            $request->text = Yii::t('main', 'К сожалению, сегодня {entity} не предоставляет следующие продукты:', [
                    'entity' => $model->entity->name
                ]) . PHP_EOL;
            $request->text .= $removedItems;
            $request->text .= Yii::t('main', '<b>Ваша корзина была очищена!</b>') . PHP_EOL;
            $request->text .= Yii::t('main', 'Может закажете что-то другое? ') . "\u{263A}";
            $request->send();
        }
    }

    public static function sendOrderTime($model)
    {
        $user = BotUsers::findOne($model->user_id);
        $api = Yii::$app->botClient;
        if ($model->payment_type != Orders::PAY_CASH) {
            if ($model->order_type == Orders::TYPE_SELF) {
                $request = $api->sendMessage();
                $request->chat_id = $user->telegram_id;
                $request->text = Yii::t('main', 'Ваш заказ будет обработан через {time} минут', ['time' => $model->period]) . PHP_EOL;
                $request->send();
            }
        } else {
            $state = new StatesLog();
            $state->log($user->id, StatesLog::MAIN_MENU, null, 'afterPhoneSent');
            if ($model->order_type == Orders::TYPE_SELF) {
                $request = $api->sendMessage();
                $request->chat_id = $user->telegram_id;
                $request->parse_mode = "HTML";
                $request->text = Yii::t('main', 'Спасибо за использование нашего сервиса!') . PHP_EOL;
                $request->text .= Yii::t('main', 'Ваш заказ будет обработан через {time} минут', ['time' => $model->period]) . PHP_EOL;
                $keyboard = new ReplyKeyboardMarkup();
                $keyboard->keyboard = [
                    [['text' => "\u{1F37D} " . Yii::t('main', 'Заказать')]],
                    [['text' => "\u{1F6D2} " . Yii::t('main', 'Корзина')]],
                    [['text' => "\u{1F696} " . Yii::t('main', 'Условия доставки')]],
                    [['text' => "\u{2699} " . Yii::t('main', 'Настройки')]],
                ];
                $request->reply_markup = $keyboard;
                $request->send();
            } else {
                $request = $api->sendMessage();
                $request->chat_id = $user->telegram_id;
                $request->parse_mode = "HTML";
                $request->text = Yii::t('main', 'Спасибо за использование нашего сервиса!') . PHP_EOL;
                $request->text .= Yii::t('main', 'Мы доставим ваш заказ как можно скорее!') . PHP_EOL;
                $request->text .= Yii::t('main', 'В то же время, мы будем уведомлять вас на каждом этапе процесса вашего заказа!');
                $keyboard = new ReplyKeyboardMarkup();
                $keyboard->keyboard = [
                    [['text' => "\u{1F37D} " . Yii::t('main', 'Заказать')]],
                    [['text' => "\u{1F6D2} " . Yii::t('main', 'Корзина')]],
                    [['text' => "\u{1F696} " . Yii::t('main', 'Условия доставки')]],
                    [['text' => "\u{2699} " . Yii::t('main', 'Настройки')]],
                ];
                $request->reply_markup = $keyboard;
                $request->send();
            }
        }
    }

    public static function sendPush($basket, $order, $channel)
    {
        $items = [];
        $itemsForPush = '';
        foreach ($basket as $item) {
            $items[] = [
                'food_id' => $item->food->id,
                'food_title' => $item->food->title,
                'food_amount' => $item->amount,
                'checked' => true
            ];
            $itemsForPush .= $item->food->title . ' x ' . $item->amount . ';' . PHP_EOL;
        }
        $orders = [
            'id' => $order->id,
            'totalPrice' => $order->totalPrice,
            'status' => [
                'code' => $order->status,
                'title' => $order->getStatuses()[$order->status]
            ],
            'payment_type' => [
                'code' => $order->payment_type,
                'title' => $order->getPaymentTypes()[$order->payment_type]
            ],
            'items' => $items,
        ];
        $links = UserRoleLink::findAll(['entity_id' => $order->entity_id]);

        foreach ($links as $link) {
            $devices = Devices::find()->select('push_token')->andWhere(['user_id' => $link->user_id, 'user_type' => Devices::ENTITY])->distinct()->all();

            foreach ($devices as $device) {
                if ($device->push_token) {
                    try {
                        SendPushNotification::send($device->push_token, $orders, $itemsForPush, $channel);
                    } catch (\Exception $e) {
                        $log = new ErrorsLog();
                        $log->message = 'Device deleted: ' . json_encode($device->getAttributes()) . ' Reason: ' . $e->getMessage();
                        $log->save(false);
                        $device->delete();
                        continue;
                    }
                }
            }
        }
    }

    public static function sendPushDriver($order_id, $channel)
    {
//        shell_exec('php /var/www/html/yii push/send-driver ' . $order_id . ' ' .$channel . ' > /dev/null 2>/dev/null &');
        $order = OrdersSearch::findOne($order_id);
        $links = Drivers::find()->andWhere(['status' => DriversSearch::ACTIVE])->all();
        $orders = [
            'id' => $order->id,
            'totalPrice' => $order->totalPrice,
            'status' => [
                'code' => $order->status,
                'title' => $order->getStatuses()[$order->status]
            ],
            'payment_type' => [
                'code' => $order->payment_type,
                'title' => $order->getPaymentTypes()[$order->payment_type]
            ],
        ];
        foreach ($links as $link) {
            $devices = Devices::find()->select('push_token')->andWhere(['user_id' => $link->id, 'user_type' => Devices::DRIVERS])->distinct()->all();

            foreach ($devices as $device) {
                if ($device->push_token) {
                    try {
                        SendPushNotification::send($device->push_token, $orders, 'Новый заказ!', $channel);
                    } catch (\Exception $e) {
                        $log = new ErrorsLog();
                        $log->message = 'Device deleted: ' . json_encode($device->getAttributes()) . ' Reason: ' . $e->getMessage();
                        $log->save(false);
                        $device->delete();
                        continue;
                    }
                }
            }
        }
    }
}