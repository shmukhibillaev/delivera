<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 1/23/2019
 * Time: 10:34 PM
 */

namespace common\helpers;


use Yii;

class SendPushNotification
{
    public static function send($token, $data, $body, $channel = 'new')
    {
        $interestDetails = [$token, $token];
        $expo = \ExponentPhpSDK\Expo::normalSetup();
        $expo->subscribe($interestDetails[0], $interestDetails[1]);
        $notification = [
//            'body' => Yii::t('main', 'Новый заказ!'),
            'body' => $body,
            'data' => json_encode($data),
            "sound" => "default",
            "priority" => "high",
            "channelId" => $channel, // android 8.0 later
            "vibrate" => [0, 250, 250, 250],
//            "title" => "Delivera",
        ];

        return $expo->notify($interestDetails[0], $notification);

    }
}