<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 1/19/2019
 * Time: 5:26 PM
 */

namespace common\helpers;


use api\base\Curl;
use Yii;

class SendSMSHelper
{
    const EMAIL = 'abduraufsherkulov@gmail.com';
    const PASSWORD = 'ajBjDhCWPXzOJHZjnwz3mKnqygpHMiDbs4PQBMn8';

    public static function getToken()
    {
        $curl = new Curl();
        $curl->setOption(CURLOPT_POSTFIELDS, ['email' => self::EMAIL, 'password' => self::PASSWORD]);
        $result = json_decode($curl->post("https://notify.eskiz.uz/api/auth/login"));
        return $result->data->token;
    }

    public static function sendSMS($phone, $message)
    {
        $curl = new Curl();

        $cache = Yii::$app->cache;

        if($cache->get('token_sms')) {
            $curl->setOption(CURLOPT_HTTPHEADER, ['Authorization: Bearer' . $cache->get('token_sms')]);
        }else{
            $token =  self::getToken();
            $cache->set('token_sms', $token, 82800);
            $curl->setOption(CURLOPT_HTTPHEADER, ['Authorization: Bearer' . $token]);
        }
        $curl->setOption(CURLOPT_POSTFIELDS, ['mobile_phone' => $phone, 'message' => $message]);
        $result = $curl->post("https://notify.eskiz.uz/api/message/sms/send");
        return $result;
    }
}