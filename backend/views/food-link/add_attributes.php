<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/4/2018
 * Time: 9:30 PM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FoodLink */

$this->title = Yii::t('main', 'Add Attributes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'group_id')->widget(Select2::classname(), [
    'data' => $model->getGroups(),
    'options' => ['placeholder' => 'Select a attribute group ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('Group Id') ?>

<?= $form->field($model, 'attribute_id')->widget(Select2::className(), [
    'data' => $model->getAttributeTitles(),
    'options' => ['prompt' => \Yii::t('main', 'Выберите ...'),],
    'pluginOptions' => [
        'multiple'=>true,
        'allowClear' => true
    ]
]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>