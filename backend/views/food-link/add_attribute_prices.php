<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/4/2018
 * Time: 9:30 PM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FoodLink */

$this->title = Yii::t('main', 'Add Attribute Prices');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin(); ?>

<?php
foreach ($model as $key => $value) {
    echo Html::label($key);
    echo Html::radioList($key, null, $value);
}
?>
    <p></p>
<?php
echo Html::label('Price');
echo Html::textInput('price');
?>
    <p></p>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>