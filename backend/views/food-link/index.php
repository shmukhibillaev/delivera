<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FoodLinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Food Links');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-link-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Food Link'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('main', 'Duplicate'), ['duplicate'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a(Yii::t('main', 'Visualize'), ['visualize'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            [
//                'attribute' => 'entity_id',
//                'value' => 'entity.name',
//                'filter' => Select2::widget([
//                    'data' => $searchModel->getEntities(),
//                    'name' => Html::getInputName($searchModel, 'entity_id'),
//                    'value' => 'entity.name',
//                    'options' => [
//                        'placeholder' => 'Select Entity',
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//                ]),
//            ],
            [
                'attribute' => 'entity_id',
                'value' => 'entity.name',
                'filter' => $searchModel->getEntities(),
            ],
//            [
//                'attribute' => 'category_id',
//                'value' => 'category.title',
//                'filter' => Select2::widget([
//                    'data' => $searchModel->getCategories(),
//                    'name' => Html::getInputName($searchModel, 'category_id'),
//                    'value' => 'category.title',
//                    'options' => [
//                        'placeholder' => 'Select Category',
//                    ],
//                    'pluginOptions' => [
//                        'allowClear' => true
//                    ],
//                ]),
//            ],
            [
                'attribute' => 'category_id',
                'value' => 'category.title',
                'filter' => $searchModel->getCategories(),
            ],
            [
                'attribute' => 'food_id',
                'value' => 'food.title',
                'filter' => Select2::widget([
                    'data' => $searchModel->getFoods(),
                    'name' => Html::getInputName($searchModel, 'food_id'),
                    'value' => 'food.title',
                    'options' => [
                        'placeholder' => 'Select Food',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
            ],
            'price',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    $result = [0 => "Неактивный", 1 => "Активный"];
                    return $result[$model->active];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
            [
                'value' => function ($model) {
                    return Html::a('Add Attributes', ['add-attributes', 'id' => $model->id]) . ' ' . Html::a('Add Attribute Prices', ['add-attribute-prices', 'id' => $model->id]);
                },
                'label' => 'Attribute Actions',
                'format' => 'raw'
            ],
        ],
    ]); ?>
</div>
