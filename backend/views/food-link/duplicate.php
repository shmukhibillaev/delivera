<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/4/2018
 * Time: 9:30 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FoodLink */

$this->title = Yii::t('main', 'Duplicate Food Link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'entity_id')->widget(Select2::classname(), [
    'data' => $model->getEntities(),
    'options' => ['placeholder' => 'Select a entity ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('From Entity') ?>

<?= $form->field($model, 'target_entity_id')->widget(Select2::classname(), [
    'data' => $model->getEntities(),
    'options' => ['placeholder' => 'Select a target entity ...'],
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => true
    ],
])->label('Target Entity') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Duplicate'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>