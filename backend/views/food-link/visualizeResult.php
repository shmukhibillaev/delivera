<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $link common\models\FoodLink */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<h2><?= $model->entity->name?></h2>

<table>
    <tr>
        <th>Category</th>
        <th>Food</th>
        <th>Price (UZS)</th>
    </tr>

    <?php foreach ($links as $link): ?>
        <tr>
            <td><?= $link->category->title ?></td>
            <td><?= $link->food->title ?></td>
            <td><?= $link->price ?></td>
        </tr>
    <?php endforeach; ?>

</table>



