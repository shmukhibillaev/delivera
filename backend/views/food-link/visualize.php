<?php
/**
 * Created by PhpStorm.
 * User: Shamsidinkhon
 * Date: 12/4/2018
 * Time: 9:30 PM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FoodLink */

$this->title = Yii::t('main', 'Visualize');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>


<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'entity_id')->widget(Select2::classname(), [
    'data' => $model->getEntities(),
    'options' => ['placeholder' => 'Select a entity ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
])->label('From Entity') ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Visualize'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>