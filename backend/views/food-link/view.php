<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FoodLink */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Food Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="food-link-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('main', 'Create Food Link'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Add Attributes', ['add-attributes', 'id' => $model->id]);?>
        <?= Html::a('Add Attribute Prices', ['add-attribute-prices', 'id' => $model->id]);?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'entity.name',
            'category.title',
            'food.title',
            'price',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    $result = [0 => "Неактивный", 1 => "Активный"];
                    return $result[$model->active];
                }
            ],
            [
                'label' => 'Attributes',
                'value' => function ($model) {
                    return $model->getAttributeLinks();
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
