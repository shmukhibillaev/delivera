<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DomainCategory */

$this->title = Yii::t('main', 'Create Domain Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Domain Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="domain-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
