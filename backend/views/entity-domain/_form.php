<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\EntityDomain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entity-domain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_domain_id')->widget(Select2::classname(), [
        'data' => $model->getCategoryDomains(),
        'options' => ['placeholder' => 'Select a domain category ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
