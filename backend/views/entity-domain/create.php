<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EntityDomain */

$this->title = Yii::t('main', 'Create Entity Domain');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Entity Domains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-domain-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
