<?php

/* @var $this yii\web\View */
/* @var int $orderAveragePrice  */

use yii\helpers\Url;

$this->title = 'Statistics';
?>
<style>
    .card-counter {
        height: 60px;
    }

    .card-counter .count-name {
        position: unset !important;
        text-align: center;
        opacity: 1;
    }
</style>
<h4>Average order price: <?= number_format($orderAveragePrice) ?> UZS</h4>
<div class="site-index">

    <div class="row">
        <div class="col-md-3">
            <a href="<?= Url::to(['statistics/entity']) ?>">
                <div class="card-counter primary">
                    <span class="count-name">Top entity</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['statistics/top-food']) ?>">
                <div class="card-counter danger">
                    <span class="count-name">Top food</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['statistics/top-users']) ?>">
                <div class="card-counter success">
                    <span class="count-name">Top users</span>
                </div>
            </a>
        </div>

    </div>

</div>
