<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Drivers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Drivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="drivers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'pass_serial',
            [
                'label' => 'File to download',
                'value' => function ($model) {
                    if ($model->pass_file)
                        return Html::a('Download', ['drivers/download', 'path' => $model->pass_file]);
                    else
                        return 'Нет документа';
                },
                'format' => 'raw'
            ],
            'full_name',
            'phone',
            'status',
            'car_number',
            'car_model',
            [
                'attribute' => 'region',
                'value' => function ($model) {
                    return $model->getRegions()[$model->region];
                },
            ],
            [
                'attribute' => 'districts',
                'value' => function ($model) {
                    $arr = $model->getDistricts();
                    $districts = explode(',', $model->districts);
                    $result = '';
                    foreach ($districts as $item) {
                        $result .= $arr[$item] . '<br>';
                    }
                    return $result;
                },
                'format' => 'raw'
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
