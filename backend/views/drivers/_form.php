<?php

use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Drivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'autocomplete' => 'new-password']) ?>
    <?php endif; ?>

    <?= $form->field($model, 'pass_serial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>

    <?= $form->field($model, 'car_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region')->dropDownList($model->getRegions(), ['id' => 'region_dep', 'prompt' => Yii::t('main', 'Выберите ...')]) ?>

    <?= $form->field($model, 'districts')->widget(DepDrop::className(), [
        'options' => ['prompt' => \Yii::t('main', 'Выберите ...'),],
        'type'=>DepDrop::TYPE_SELECT2,
        'select2Options'=>['pluginOptions'=>['multiple'=>true]],
        'pluginOptions' => [
            'depends' => ['region_dep'],
            'url' => \yii\helpers\Url::to(['/site/region-children']),
            'params' => [\Yii::$app->getRequest()->csrfParam],
            'placeholder' => \Yii::t('main', 'Выберите ...'),
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
