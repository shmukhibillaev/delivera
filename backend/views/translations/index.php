<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TranslationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Translations');
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->session->getFlash('updateTranslateFiles'))
    echo \yii\bootstrap\Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => Yii::t('main', 'File with translates updated successfully!'),
    ]);
?>
<div class="translations-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Translations'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('main', 'Update Translations'), ['update-all'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'key_word:ntext',
            'uz:ntext',
            'ru:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
