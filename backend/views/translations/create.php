<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Translations */

$this->title = Yii::t('main', 'Create Translations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
