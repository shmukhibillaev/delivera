<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Translations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="translations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key_word')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'uz')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ru')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
