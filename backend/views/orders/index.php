<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return @$model->user->last_name . ' ' . @$model->user->first_name;
                }
            ],
            'entity.name',
            'totalPrice',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatuses()[$model->status];
                }
            ],
            //'period',
            //'driver_id',
            //'confirm_code',
            //'created_at',
            //'updated_at',
            //'order_type',
            //'payment_type',
            //'user_location',
            //'user_location_distance',
            //'user_location_text:ntext',
            //'map_info:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'

            ],
        ],
    ]); ?>
</div>
