<?php

use common\helpers\GeneralHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = 'Заказ: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Список заказов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1>Номер заказа: <?= Html::encode($model->id) ?></h1>
    <h3>Данные пользователя:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->last_name . ' ' . $model->user->first_name;
                },
                'label' => 'Ф.И.О пользователя'
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->phone;
                },
                'label' => 'Телефон номер пользователя'
            ],
            [
                'attribute' => 'user_location_distance',
                'value' => function ($model) {
                    return $model->user_location_distance . ' km';
                },
                'label' => 'Дистанция'
            ],
            [
                'attribute' => 'user_location',
                'value' => function ($model) {
                    $ll = explode(',', $model->user_location);
                    return Html::a('https://maps.google.com/?ll=' . $ll[0].','.$ll[1], 'https://maps.google.com/?ll=' . $ll[0].','.$ll[1]);
                },
                'format' => 'raw',
                'label' => 'Локация'
            ],
            [
                'attribute' => 'user_location_text',
                'format' => 'text',
                'label' => 'Ориентир'
            ],
//            'map_info:ntext',
        ],
    ]) ?>
    <p></p>
    <h3>Данные заказа:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'created_at',
                'label' => 'Дата заказа'
            ],
            [
                'attribute' => 'entity.name',
                'label' => 'Название учреждения'
            ],
            [
                'value' => function ($model) {
                    return $model->getFoods();
                },
                'label' => 'Список еды',
                'format' => 'raw'
            ],
            [
                'attribute' => 'totalPrice',
                'value' => function ($model) {
                    return $model->totalPrice . ' UZS';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatuses()[$model->status];
                }
            ],
            [
                'attribute' => 'payment_type',
                'value' => function ($model) {
                    return $model->getPaymentTypes()[$model->payment_type];
                },
                'label' => 'Тип оплаты',
            ],
//            'period',
        ],
    ]) ?>
    <p></p>
    <h3>Данные водителя:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'driver.full_name',
                'label' => 'Ф.И.О водителя'
            ],
            [
                'label' => 'Сумма доставки',
                'value' => function ($model) {
                    return GeneralHelper::calculateDelivery($model->user_location_distance) . ' UZS';
                }
            ],
            [
                'attribute' => 'driver.phone',
                'label' => 'Номер телефона'
            ],
        ],
    ]) ?>

</div>
