<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="site-signup">

    <p>Please fill out the following fields to add access for user:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                'data' => \common\models\UserRoleLink::getUsernames(),
                'options' => ['placeholder' => 'Select a username ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Select Username') ?>


            <?= $form->field($model, 'entity_id')->widget(Select2::classname(), [
                'data' => \common\models\FoodLink::getEntities(),
                'options' => ['placeholder' => 'Select a entity ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Select Entity') ?>

            <?= $form->field($model, 'role_id')->widget(Select2::classname(), [
                'data' => \common\models\UserRoleLink::getRoles(),
                'options' => ['placeholder' => 'Select a role ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Select Role') ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
