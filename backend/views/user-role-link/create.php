<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserRoleLink */

$this->title = Yii::t('main', 'Create User Role Link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'User Role Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-link-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('form', [
        'model' => $model,
    ]) ?>

</div>
