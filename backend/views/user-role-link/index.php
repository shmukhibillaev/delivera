<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserRoleLinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'User Role Links');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-link-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Add access'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user.username',
            [
                'attribute' => 'role_id',
                'value' => function ($model) {
                    return $model->getRoles()[$model->role_id];
                }
            ],
            'entity.name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
