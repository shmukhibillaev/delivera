<?php

use common\models\Drivers;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Entity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput() ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'responsible_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'entity_domain_id')->widget(Select2::classname(), [
        'data' => $model->getEntityDomains(),
        'options' => ['placeholder' => 'Select a domain ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'works_from')->widget(TimePicker::classname(), [
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 1,
            'secondStep' => 5,
        ]
    ]) ?>

    <?= $form->field($model, 'works_to')->widget(TimePicker::classname(), [
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 1,
            'secondStep' => 5,
        ]
    ]) ?>

    <?= $form->field($model, 'active')->dropDownList([
            0 => 'NO',
            1 => 'YES'
    ]) ?>

    <?= $form->field($model, 'region_id')->dropDownList(Drivers::getRegions(), ['id' => 'region_dep', 'prompt' => Yii::t('main', 'Выберите ...')]) ?>

    <?= $form->field($model, 'district_id')->widget(DepDrop::className(), [
        'options' => ['prompt' => \Yii::t('main', 'Выберите ...'),],
//        'type'=>DepDrop::TYPE_SELECT2,
//        'select2Options'=>['pluginOptions'=>['multiple'=>true]],
        'pluginOptions' => [
            'depends' => ['region_dep'],
            'url' => \yii\helpers\Url::to(['/site/region-children']),
            'params' => [\Yii::$app->getRequest()->csrfParam],
            'placeholder' => \Yii::t('main', 'Выберите ...'),
        ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
