<?php

use common\models\Drivers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Entity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Entities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('main', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('main', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('main', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'longitude',
            'latitude',
            'phone',
            'responsible_person',
            [
                    'label' => 'Entity Domain',
                    'attribute' => 'entityDomain.name',
            ],
            'works_from',
            'works_to',
            'works_to',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    $arr = [0 => "NO", 1 => "YES"];
                    return $arr[$model->active];
                }
            ],
            [
                'attribute' => 'region_id',
                'value' => function ($model) {
                    return Drivers::getRegions()[$model->region_id];
                },
            ],
            [
                'attribute' => 'district_id',
                'value' => function ($model) {
                    $arr = Drivers::getDistricts();
                    $districts = explode(',', $model->district_id);
                    $result = '';
                    foreach ($districts as $item) {
                        $result .= $arr[$item] . '<br>';
                    }
                    return $result;
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
