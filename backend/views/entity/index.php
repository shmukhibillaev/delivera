<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EntitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('main', 'Entities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entity-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('main', 'Create Entity'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
//            'longitude',
//            'latitude',

            [
                'label' => 'Domain Name',
                'value' => 'entityDomain.name',
            ],
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    $arr = [0 => "NO", 1 => "YES"];
                    return $arr[$model->active];
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],


        ],
    ]); ?>
</div>
