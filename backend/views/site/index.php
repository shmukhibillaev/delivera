<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="row">
        <div class="col-md-3">
            <a href="<?= Url::to(['photos/index']) ?>">
                <div class="card-counter primary">
                    <i class="fa fa-images"></i>
                    <span class="count-numbers"><?= \common\models\Photos::find()->count() ?></span>
                    <span class="count-name">Add Photo</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['domain-category/index']) ?>">
                <div class="card-counter danger">
                    <i class="fa fa-plus-circle"></i>
                    <span class="count-numbers"><?= \common\models\DomainCategory::find()->count() ?></span>
                    <span class="count-name">Add Category Domain</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['entity-domain/index']) ?>">
                <div class="card-counter success">
                    <i class="fa fa-plus"></i>
                    <span class="count-numbers"><?= \common\models\EntityDomain::find()->count() ?></span>
                    <span class="count-name">Add Entity Domain</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['user/index']) ?>">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers"><?= \common\models\User::find()->count()?></span>
                    <span class="count-name">Add User</span>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <a href="<?= Url::to(['entity/index']) ?>">
                <div class="card-counter secondary">
                    <i class="fa fa-building"></i>
                    <span class="count-numbers"><?= \common\models\Entity::find()->count() ?></span>
                    <span class="count-name">Add Entity</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['category/index']) ?>">
                <div class="card-counter warning">
                    <i class="fa fa-folder-open"></i>
                    <span class="count-numbers"><?= \common\models\Category::find()->count() ?></span>
                    <span class="count-name">Add Category</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['food/index']) ?>">
                <div class="card-counter dark">
                    <i class="fa fa-utensils"></i>
                    <span class="count-numbers"><?= \common\models\Food::find()->count() ?></span>
                    <span class="count-name">Add Food</span>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::to(['food-link/index']) ?>">
                <div class="card-counter white">
                    <i class="fa fa-link"></i>
                    <span class="count-numbers"><?= \common\models\FoodLink::find()->count()?></span>
                    <span class="count-name">Add Food Link</span>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <a href="<?= Url::to(['drivers/index']) ?>">
                <div class="card-counter success">
                    <i class="fa fa-car"></i>
                    <span class="count-numbers"><?= \common\models\Drivers::find()->count() ?></span>
                    <span class="count-name">Add Driver</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="<?= Url::to(['user-role-link/index']) ?>">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers"><?= \common\models\UserRoleLink::find()->andWhere(['role_id' => \common\models\UserRoleLink::MANAGER])->count()?></span>
                    <span class="count-name">Add User Access</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="<?= Url::to(['translations/index']) ?>">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers"><?= \common\models\Translations::find()->count()?></span>
                    <span class="count-name">Add Translation</span>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="<?= Url::to(['statistics/index']) ?>">
                <div class="card-counter info">
                    <i class="fa fa-info"></i>
                    <span class="count-name">Statistics</span>
                </div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <a href="<?= Url::to(['orders/index']) ?>">
                <div class="card-counter primary">
                    <i class="fa fa-car"></i>
                    <span class="count-numbers"><?= \common\models\Orders::find()->count() ?></span>
                    <span class="count-name">Orders</span>
                </div>
            </a>
        </div>
    </div>

</div>
