<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

<!--                --><?//= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'entity_id')->widget(Select2::classname(), [
                'data' => \common\models\FoodLink::getEntities(),
                'options' => ['placeholder' => 'Select a entity ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Select Entity') ?>

            <?= $form->field($model, 'role_id')->widget(Select2::classname(), [
                'data' => \common\models\UserRoleLink::getRoles(),
                'options' => ['placeholder' => 'Select a role ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Select Role') ?>

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
