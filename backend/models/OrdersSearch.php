<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form of `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'entity_id', 'driver_id', 'confirm_code'], 'integer'],
            [['totalPrice', 'status', 'period', 'created_at', 'updated_at', 'order_type', 'payment_type', 'user_location', 'user_location_distance', 'user_location_text', 'map_info'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'entity_id' => $this->entity_id,
            'driver_id' => $this->driver_id,
            'confirm_code' => $this->confirm_code,
        ]);

        $query->andFilterWhere(['like', 'totalPrice', $this->totalPrice])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'period', $this->period])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'updated_at', $this->updated_at])
            ->andFilterWhere(['like', 'order_type', $this->order_type])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'user_location', $this->user_location])
            ->andFilterWhere(['like', 'user_location_distance', $this->user_location_distance])
            ->andFilterWhere(['like', 'user_location_text', $this->user_location_text])
            ->andFilterWhere(['like', 'map_info', $this->map_info]);

        return $dataProvider;
    }
}
