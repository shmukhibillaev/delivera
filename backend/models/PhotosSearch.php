<?php

namespace backend\models;

use api\response\Message;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Photos;
use yii\web\UploadedFile;

/**
 * PhotosSearch represents the model behind the search form of `common\models\Photos`.
 */
class PhotosSearch extends Photos
{

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['title', 'real_path', 'file_id', 'for_command'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Photos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'real_path', $this->real_path])
            ->andFilterWhere(['like', 'file_id', $this->file_id])
            ->andFilterWhere(['like', 'for_command', $this->for_command]);

        return $dataProvider;
    }

    public function upload()
    {
        if ($this->imageFile) {
            if($this->real_path){
                unlink(Yii::getAlias('@frontend') . '/web/' . $this->real_path);
            }
            $file = Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;
            if ($this->imageFile->saveAs(Yii::getAlias('@frontend') . '/web/uploads/images/' . $file)) {
                $api = Yii::$app->botClient;
                $request = $api->sendPhoto();
                $request->chat_id = '@deliveratest';
                $request->photo = new \api\InputFile('@frontend/web/uploads/images/' . $file);
                $result = $request->send();
                if ($result instanceof Message) {
                    $fileSize = 0;
                    foreach ($result->photo as $photo) {
                        if ($photo->file_size > $fileSize) {
                            $fileSize = $photo->file_size;
                            $this->file_id = $photo->file_id;
                        }
                    }
                } else {
                    $this->addError('imageFile', 'Something went wrong! Image not uploaded to Telegram server!');
                    unlink('uploads/images/' . $file);
                    return false;
                }
                $this->real_path = 'uploads/images/' . $file;
            } else {
                $this->addError('imageFile', 'Something went wrong! Image not uploaded!');
                return false;
            }
        }
        $this->save(false);
        return true;
    }
}
