<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Drivers;
use yii\web\UploadedFile;

/**
 * DriversSearch represents the model behind the search form of `common\models\Drivers`.
 */
class DriversSearch extends Drivers
{
    public $imageFile;

    const ACTIVE = 'active';
    const BANNED = 'banned';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['username', 'phone', 'status', 'pass_serial', 'password', 'full_name', 'car_number', 'car_model'], 'safe'],
            [['username', 'password', 'region', 'districts'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Drivers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'pass_serial', $this->pass_serial])
            ->andFilterWhere(['like', 'pass_file', $this->pass_file])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }

    public static function getStatuses(){
        return [
            self::ACTIVE => 'Active',
            self::BANNED => 'Banned',
        ];
    }

    public function upload()
    {
        if ($this->imageFile) {
            if($this->pass_file){
                unlink($this->pass_file);
            }
            $file = Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;

            if ($this->imageFile->saveAs('uploads/driverFiles/' . $file)) {
                $this->pass_file = 'uploads/driverFiles/' . $file;
            } else {
                $this->addError('imageFile', 'Something went wrong! Image not uploaded!');
                return false;
            }
        }
        $this->save(false);
        return true;
    }

    public function beforeSave($insert)
    {
        if($insert)
            $this->password = Yii::$app->security->generatePasswordHash($this->password);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::ACTIVE]);
    }
}
