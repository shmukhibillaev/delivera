<?php

namespace backend\models;

use common\models\BotUsers;
use common\models\Orders;
use yii\data\ActiveDataProvider;

/**
 * FoodSearch represents the model behind the search form of `common\models\BotUsers`.
 */
class BotUsersSearch extends BotUsers
{
    public $amount;
    public $price;

    public function searchTopUsers()
    {
        $query = self::find()->select('u.id, u.first_name, u.last_name, u.phone, count(*) as amount, SUM(o.totalPrice) as price')->from(self::tableName() . ' u')
            ->innerJoin(Orders::tableName() . ' o', 'o.user_id=u.id')
            ->groupBy('u.id, u.first_name, u.last_name, u.phone')
            ->orderBy(['count(*)' => SORT_DESC])
            ->limit(20)
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

          return $dataProvider;
    }
}
