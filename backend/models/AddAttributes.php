<?php

namespace backend\models;

use common\models\Attributes;
use common\models\AttributesGroup;
use common\models\AttributesLink;
use yii\helpers\ArrayHelper;

/**
 * AddAttributes represents the model behind the search form of `common\models\AttributesLink`.
 */
class AddAttributes extends AttributesLink
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id'], 'integer'],
            [['attribute_id'], 'safe'],
        ];
    }

    public function getGroups(){
        return ArrayHelper::map(AttributesGroup::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'title');
    }

    public function getAttributeTitles(){
        return ArrayHelper::map(Attributes::find()->orderBy(['id' => SORT_DESC])->all(), 'id', 'title');
    }
}
