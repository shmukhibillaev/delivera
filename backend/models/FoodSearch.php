<?php

namespace backend\models;

use common\models\Basket;
use common\models\Orders;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Food;

/**
 * FoodSearch represents the model behind the search form of `common\models\Food`.
 */
class FoodSearch extends Food
{
    public $amount;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'description', 'image_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Food::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image_id', $this->image_id]);

        return $dataProvider;
    }

    public function searchTopFood()
    {
        $query = self::find()->select('f.id, f.title,  SUM(b.amount) as amount')
        ->from(self::tableName() . ' f')
        ->innerJoin(Basket::tableName() . ' b', 'b.food_id=f.id')
        ->andWhere(['b.status' => Basket::STATUS_ORDERED])
            ->groupBy('f.id, f.title')
            ->orderBy(['SUM(b.amount)' => SORT_DESC])
            ->limit(20)
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
