<?php

namespace backend\models;

use common\models\Orders;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Entity;

/**
 * EntitySearch represents the model behind the search form of `common\models\Entity`.
 */
class EntitySearch extends Entity
{
    public $sale;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'entity_domain_id', 'region_id', 'district_id', 'works_from', 'works_to', 'longitude', 'latitude'], 'integer'],
            [['region_id', 'district_id', 'works_from', 'works_to', 'longitude', 'latitude'], 'required'],
            [['name', 'phone', 'responsible_person', 'active'], 'safe'],
            [['longitude', 'latitude'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['active' => SORT_DESC]
            ]
        ]);

        $this->load($params);
        if (!$this->validate(['id', 'name', 'entity_domain_id', 'active'])) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'entity_domain_id' => $this->entity_domain_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'responsible_person', $this->responsible_person])
            ->andFilterWhere(['like', 'works_from', $this->works_from])
            ->andFilterWhere(['like', 'works_to', $this->works_to]);

        return $dataProvider;
    }
    public function searchTopEntity()
    {
        $query = self::find()
            ->select('e.id, e.name, SUM(o.totalPrice) as sale')->from(Entity::tableName() . ' e')
            ->innerJoin(Orders::tableName() . ' o', 'o.entity_id = e.id')
            ->andWhere(['e.active' => true])
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['active' => SORT_DESC]
            ]
        ]);

        return $dataProvider;
    }
}
