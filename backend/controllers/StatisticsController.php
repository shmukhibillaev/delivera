<?php

namespace backend\controllers;

use backend\models\BotUsersSearch;
use backend\models\EntitySearch;
use backend\models\FoodSearch;
use backend\models\UserSearch;
use common\models\BotUsers;
use common\models\Orders;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatisticsController implements the CRUD actions for Statistics model.
 */
class StatisticsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $orderPrice = Orders::find()->select('AVG(totalPrice)')->column();
        return $this->render('index', [
            'orderAveragePrice' => isset($orderPrice[0]) ? $orderPrice[0] : 0,
        ]);
    }

    public function actionEntity()
    {
        $searchModel = new EntitySearch();
        $dataProvider = $searchModel->searchTopEntity();

        return $this->render('entity', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTopFood()
    {
        $searchModel = new FoodSearch();
        $dataProvider = $searchModel->searchTopFood();

        return $this->render('food', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTopUsers(){
        $searchModel = new BotUsersSearch();
        $dataProvider = $searchModel->searchTopUsers();

        return $this->render('users', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
