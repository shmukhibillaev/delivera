<?php

namespace backend\controllers;

use Yii;
use common\models\Translations;
use backend\models\TranslationsSearch;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TranslationsController implements the CRUD actions for Translations model.
 */
class TranslationsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Translations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TranslationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Translations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Translations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Translations();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Translations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Translations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Translations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Translations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Translations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }

    public function actionUpdateAll()
    {
        $data = [];
        /** @var Translations $model */
        foreach (Translations::find()->all() as $model) {
            $data['uz'][$model->key_word] = $model->uz;
            $data['ru'][$model->key_word] = $model->ru;
        }

        foreach (['@frontend'] as $alias) {
            foreach ($data as $lang => $pairs) {
                $path = Yii::getAlias($alias) . "/messages/{$lang}/";
                if (!file_exists($path))
                    @mkdir($path, 0777, true);

                $pathToFile = $path . "main.php";
                $content = '<?php ' . PHP_EOL . 'return [' . PHP_EOL;
                foreach ($pairs as $key => $translates) {
                    $content .= "    '{$key}' => '{$translates}'," . PHP_EOL;
                }
                $content .= '];';
                file_put_contents($pathToFile, $content, LOCK_EX);
            }
        }

        Yii::$app->session->setFlash('updateTranslateFiles', true);
        return $this->redirect(['index']);
    }
}
