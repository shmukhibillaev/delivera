<?php

namespace backend\controllers;

use common\models\RegionList;
use common\models\UserRoleLink;
use frontend\models\SignupForm;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'add-user', 'add-access', 'region-children'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionAddUser()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
//                if (Yii::$app->getUser()->login($user)) {
                return $this->redirect(['user/index']);
//                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionAddAccess()
    {
        $model = new UserRoleLink();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                return $this->goHome();
        }

        return $this->render('access', [
            'model' => $model,
        ]);
    }

    public function actionRegionChildren()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null && isset($parents[0]) && !empty($parents[0])) {
//                $lang = empty(Yii::$app->language) ? 'ru' : Yii::$app->language;
                $regionId = $parents[0];
                $out = RegionList::find()
                    ->select('parent_id AS id, title AS name')
                    ->where('soato_id = :region_id')
//                    ->where('soato_id = :region_id AND lang = :lang AND parent_id < 10000000')
                    ->params([
                        ':region_id' => $regionId,
//                        'lang' => $lang,
                    ])
                    ->orderBy(['title' => SORT_ASC])
                    ->asArray()->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

}
