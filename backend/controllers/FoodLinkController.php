<?php

namespace backend\controllers;

use backend\models\AddAttributes;
use common\models\AttributesLink;
use common\models\AttributesPrice;
use Yii;
use common\models\FoodLink;
use backend\models\FoodLinkSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FoodLinkController implements the CRUD actions for FoodLink model.
 */
class FoodLinkController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FoodLink models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FoodLinkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FoodLink model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FoodLink model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FoodLink();
        $lastOne = FoodLink::find()->orderBy(['id' => SORT_DESC])->one();
        $model->category_id = @$lastOne->category_id;
        $model->entity_id = @$lastOne->entity_id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FoodLink model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FoodLink model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FoodLink model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FoodLink the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FoodLink::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('main', 'The requested page does not exist.'));
    }

    public function actionDuplicate()
    {
        $model = new FoodLink();
        if ($model->load(Yii::$app->request->post())) {
            $model->duplicate();
            return $this->redirect(['index']);
        }

        return $this->render('duplicate', [
            'model' => $model,
        ]);
    }

    public function actionVisualize()
    {
        $model = new FoodLink();
        if ($model->load(Yii::$app->request->post())) {
            $links = FoodLink::find()->andWhere(['entity_id' => $model->entity_id])->with(['food', 'category'])->all();
            return $this->render('visualizeResult', [
                'links' => $links,
                'model' => $model
            ]);
        }

        return $this->render('visualize', [
            'model' => $model,
        ]);

    }

    public function actionAddAttributes($id)
    {
        $foodLink = $this->findModel($id);
        $model = new AddAttributes();
        if ($model->load(Yii::$app->request->post())) {
            if($model->attribute_id && $model->group_id){
                foreach ($model->attribute_id as $item){
                    $new = new AttributesLink();
                    $new->attribute_id = $item;
                    $new->group_id = $model->group_id;
                    $new->food_id = $foodLink->food_id;
                    $new->entity_id = $foodLink->entity_id;
                    $new->save();
                }
            }
            return $this->redirect(['view', 'id' => $foodLink->id]);
        }

        return $this->render('add_attributes', [
            'model' => $model,
        ]);

    }

    public function actionAddAttributePrices($id)
    {
        $foodLink = $this->findModel($id);
        $attributes = AttributesLink::find()->andWhere(['entity_id' => $foodLink->entity_id, 'food_id' => $foodLink->food_id])->orderBy(['attribute_id' => SORT_ASC])->all();
        $result = [];
        foreach ($attributes as $attribute){
            $result[$attribute->group->title][$attribute->attribute_id]= $attribute->attr->title;
        }
        if (Yii::$app->request->post()){
            $posts = Yii::$app->request->post();
            unset($posts['_csrf-backend']);
            $new = new AttributesPrice();
            $new->food_id = $foodLink->food_id;
            $new->entity_id = $foodLink->entity_id;
            $new->price = $posts['price'];
            unset($posts['price']);
            $new->combination = implode('', $posts);
            $new->attributes_data = json_encode(array_values($posts));
            $new->save();
            return $this->redirect(['view', 'id' => $foodLink->id]);
        }

        return $this->render('add_attribute_prices', [
            'model' => $result,
        ]);

    }

}
